<?php

/**
 * Add a rewrite rule to accept GET arguments on the search results page
 */
add_filter('init', function() {
  add_rewrite_rule(
    // The resulting URL with regex to match the incoming arguments
    'search/([^/]*)/([^/]*)/([^/]*)/?',
    // The expected URL
    'index.php?pagename=search-results&param1=$matches[1]&param2=$matches[2]&param3=$matches[3]',
    // This is a rather specific URL, so we add it to the top of the list
    // Otherwise, the "catch-all" rules at the bottom (for pages and attachments) will "win"
    'top' );
});

function replace_core_jquery_version() {
  wp_deregister_script( 'jquery' );
  // Change the URL if you want to load a local copy of jQuery from your own server.
  wp_register_script( 'jquery', "https://code.jquery.com/jquery-3.5.1.min.js", array(), '3.5.1' );
}

add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );

/* Single Customers */
function create_post_type_customers() {
    register_post_type( 'customers', [
        'labels' => [
          'name' => "Clientes",
          'singular_name' => "Cliente"
        ],
        'public' => true,
        'has_archive' => true,
        'rewrite' => array('slug' => 'clientes'),
        'menu_icon'   => 'dashicons-buddicons-buddypress-logo',
        'supports' => array('author')
      ]
    );
  }
  add_action( 'init', 'create_post_type_customers' );

/* Single Units */
function create_post_type_units() {
  register_post_type( 'units', [
      'labels' => [
        'name' => "Unidades",
        'singular_name' => "Unidade"
      ],
      'public' => true,
      'has_archive' => true,
      'rewrite' => array( 'slug' => 'unidades'),
      'show_in_rest' => true,
      'menu_icon'   => 'dashicons-store',
      'supports' => array('author')
    ]
  );
}
add_action( 'init', 'create_post_type_units' );

/* Single States */
function create_post_type_states() {
  register_post_type( 'states', [
      'labels' => [
        'name' => "Estados",
        'singular_name' => "Estado"
      ],
      'public' => true,
      'has_archive' => true,
      'rewrite' => array('slug' => 'estados'),
      'show_in_rest' => true,
      'menu_icon'   => 'dashicons-location-alt',
      'supports' => array('author')
    ]
  );
}
add_action( 'init', 'create_post_type_states' );

/* Single Cities */
function create_post_type_cities() {
  register_post_type( 'cities', [
      'labels' => [
        'name' => "Cidades",
        'singular_name' => "Cidade"
      ],
      'public' => true,      
      'has_archive' => true,
      'rewrite' => array('slug' => 'cidades'),
      'show_in_rest' => true,
      'menu_icon'   => 'dashicons-admin-multisite',
      'supports' => array('author')
    ]
  );
}
add_action( 'init', 'create_post_type_cities' );

/* Single Zones */
function create_post_type_zones() {
  register_post_type( 'zones', [
      'labels' => [
        'name' => "Zonas",
        'singular_name' => "Zona"
      ],
      'public' => true,      
      'has_archive' => true,
      'rewrite' => array('slug' => 'zonas'),
      'show_in_rest' => true,
      'menu_icon'   => 'dashicons-flag',
      'supports' => array('author')
    ]
  );
}
add_action( 'init', 'create_post_type_zones' );

function cptui_register_my_cpts_depoimento() {

	/**
	 * Post Type: Depoimentos.
	 */

	$labels = [
		"name" => __( "Depoimentos", "custom-post-type-ui" ),
		"singular_name" => __( "Depoimento", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Depoimentos", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "depoimento", "with_front" => true ],
        "query_var" => true,
        "menu_icon" => "dashicons-testimonial",
		"supports" => [ "title" ],
	];

	register_post_type( "depoimento", $args );
}

add_action( 'init', 'cptui_register_my_cpts_depoimento' );

function cptui_register_my_cpts_noticia() {

	/**
	 * Post Type: Noticias.
	 */

	$labels = [
		"name" => __( "Noticias", "custom-post-type-ui" ),
		"singular_name" => __( "Noticia", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Noticias", "custom-post-type-ui" ),
		"labels" => $labels,
		"description" => "",
		"public" => true,
		"publicly_queryable" => true,
		"show_ui" => true,
		"show_in_rest" => true,
		"rest_base" => "",
		"rest_controller_class" => "WP_REST_Posts_Controller",
		"has_archive" => false,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"delete_with_user" => false,
		"exclude_from_search" => false,
		"capability_type" => "post",
		"map_meta_cap" => true,
		"hierarchical" => false,
		"rewrite" => [ "slug" => "noticia", "with_front" => true ],
		"query_var" => true,
		"supports" => [ "title", "editor", "thumbnail", "excerpt" ],
		"taxonomies" => [ "category" ],
	];

	register_post_type( "noticia", $args );
}

add_action( 'init', 'cptui_register_my_cpts_noticia' );

function cptui_register_my_taxes_categoria() {

	/**
	 * Taxonomy: Categorias.
	 */

	$labels = [
		"name" => __( "Categorias", "custom-post-type-ui" ),
		"singular_name" => __( "Categoria", "custom-post-type-ui" ),
	];

	$args = [
		"label" => __( "Categorias", "custom-post-type-ui" ),
		"labels" => $labels,
		"public" => true,
		"publicly_queryable" => true,
		"hierarchical" => true,
		"show_ui" => true,
		"show_in_menu" => true,
		"show_in_nav_menus" => true,
		"query_var" => true,
		"rewrite" => [ 'slug' => 'categoria', 'with_front' => true, ],
		"show_admin_column" => false,
		"show_in_rest" => true,
		"rest_base" => "categoria",
		"rest_controller_class" => "WP_REST_Terms_Controller",
		"show_in_quick_edit" => false,
		];
	register_taxonomy( "categoria", [ "depoimento" ], $args );
}
add_action( 'init', 'cptui_register_my_taxes_categoria' );


//Auto Add and Update Title
function post_title_updater( $post_id ) {

  $my_post = array();
  $my_post['ID'] = $post_id;

  if ( get_post_type() == 'states' ) {

    $my_post['post_title'] = get_field('states_name');
    
  } elseif ( get_post_type() == 'cities' ) {

    $my_post['post_title'] = get_field('city_name') . " - " . get_field( "states_uf", get_field('city_state'));

  } elseif ( get_post_type() == 'zones' ) {

    $my_post['post_title'] = get_field('zone_name') . " - " . get_field( "city_name", get_field('zone_city'))  . " - " . get_field( "states_uf", get_field( "city_state", get_field('zone_city')));
  
  }elseif ( get_post_type() == 'units' ) {

    if(get_field('has_zone')){
      
      $state_id = get_field( "city_state", get_field( "zone_city", get_field('unit_zone')));

      $city_id = get_field( "zone_city", get_field('unit_zone'));
      
      $title_complement = get_field( "zone_name", get_field('unit_zone')) . " - " . get_field( "city_name", $city_id) . " - " . get_field( "states_uf", $state_id);
      
      update_field('unit_state', $state_id);

      update_field('unit_city', $city_id);

    }else{
      
      $state_id = get_field( "city_state", get_field('unit_city'));

      $title_complement = get_field( "city_name", get_field('unit_city')) . " - " . get_field( "states_uf", $state_id);
      
      delete_field('unit_zone');
      
      update_field('unit_state', $state_id);
      
    }

    // $my_post['post_title'] = "Home Angels - " . $title_complement;

    // update_field('unit_name', $my_post['post_title']);

    $my_post['post_title'] = get_field('unit_name');
    $my_post['post_name'] = get_field('unit_url');

  }

  if($my_post['post_title'] != null && get_post_type() != 'units')
    $my_post['post_name'] = $my_post['post_title'];

  // Update the post into the database
  wp_update_post( $my_post );

}
 
// run after ACF saves the $_POST['fields'] data
add_action('acf/save_post', 'post_title_updater', 20);

//Google Maps API Key
function maps_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyDpEJQQ8yCmIaqnPdOcbdrZ5EdPz4QjaXA');
}

add_action('acf/init', 'maps_acf_init');

function Mask($mask,$str){

  $str = str_replace(" ","",$str);

  for($i=0;$i<strlen($str);$i++){
      $mask[strpos($mask,"#")] = $str[$i];
  }

  return $mask;

}


/** New slug link for Units */
function gp_remove_cpt_slug( $post_link, $post ) {

  if ( 'unidades' === $post->post_type && 'publish' === $post->post_status ) {
  $post_link = str_replace( '/' . $post->post_type . '/', '/', $post_link );
  }
 
  return $post_link;
 }
 add_filter( 'post_type_link', 'gp_remove_cpt_slug', 10, 2 );

 function gp_add_cpt_post_names_to_main_query( $query ) {

  // Bail if this is not the main query.
  if ( ! $query->is_main_query() ) {
  return;
  }
 
  // Bail if this query doesn't match our very specific rewrite rule.
  if ( ! isset( $query->query['page'] ) || 2 !== count( $query->query ) ) {
  return;
  }
 
  // Bail if we're not querying based on the post name.
  if ( empty( $query->query['name'] ) ) {
  return;
  }
 
  // Add Custom Post to the list of post types WP will include when it queries based on the post name.
  $query->set( 'post_type', array( 'units') );
 }
 add_action( 'pre_get_posts', 'gp_add_cpt_post_names_to_main_query' );
// Limit except length to 125 characters.
// tn limited excerpt length by number of characters
function get_excerpt( $count ) {
  $excerpt = get_field('depoimento');
  $excerpt = strip_tags($excerpt);
  $excerpt = substr($excerpt, 0, $count);
  $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
  $excerpt = $excerpt.'...<br> <a class="leia-mais" href="#" data-toggle="collapse" data-target="#depoimentodesk-'.get_the_ID().'" aria-expanded="false" aria-controls="depoimentodesk-'.get_the_ID().'"> Leia mais +</a>';
  return $excerpt;
  }
?>