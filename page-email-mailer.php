<?php

define('DONOTCACHEPAGE', true);

try {

  $customerEmail = $_GET['email'];

  $customerName = $_GET['customer_name'];

  $customerPhone = $_GET['phone'];

  $unityEmail = $_GET['unity_email'];

  $unityId = $_GET['unity_id'];

  if(!isset($customerPhone) || !isset($customerName) || !isset($customerEmail) || !isset($unityId) || !isset($unityEmail)){
    throw new Exception('Campos em branco'); 
  }

  if(strlen($customerPhone) < 10){    
    throw new Exception('Telefone inválido'); 
  }
  if(!filter_var($customerEmail, FILTER_VALIDATE_EMAIL)) {
    throw new Exception('E-mail inválido'); 
  }


  $to = $unityEmail;
  $subject = "Agendamento - Home Angels";
  $message = 'Olá Franqueado. 

  Abaixo, você está recebendo dados de um lead interessado em agendar uma avaliação gratuita. Entre em contato o quanto antes para aumentar suas chances de conversão! <br><br>
  
  NOME: [NAME] <br>
  EMAIL: [EMAIL] <br>
  TELEFONE: [PHONE] <br>';


  $message = str_replace("[NAME]", $customerName, $message);
  $message = str_replace("[PHONE]", $customerPhone, $message);
  $message = str_replace("[EMAIL]", $customerEmail, $message);

 
  $headers = array('Content-Type: text/html; charset=UTF-8');

  add_action( 'phpmailer_init', 'mailer_config', 10, 1);
  

  wp_mail($to, $subject, $message, $headers);

  //Create Customer
  $post_new_customer = array(
    'post_type' => 'customers'
  );

  $postID = wp_insert_post( $post_new_customer ); //here's the catch

  update_field('customer_name', $customerName, $postID);
  update_field('customer_email', $customerEmail, $postID);
  update_field('customer_phone', $customerPhone, $postID);
  update_field('customer_unit', $unityId, $postID);


  $result->message = true;

  echo json_encode($result);
} catch (Exception $err) {

  http_response_code(400);

  $result->message = $err->getMessage();

  echo json_encode($result);
}




?>