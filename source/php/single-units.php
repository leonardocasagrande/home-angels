<?php get_header('unity'); ?>

<section class="banner-unidade-single ">
    <div class="col-lg-10 m-auto h-100">

        <div class="unidade">

            <a class="back-button d-lg-none d-flex" href="#">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/backbutton.svg" alt="">
            </a>


            <div class="col-12 px-lg-0 px-5">
                <span><?= get_field('unit_name') ?></span>
            </div>

            <a href="#" class="icon-map d-lg-none d-flex">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/icon-map.svg" alt="">
            </a>
        </div>

        <div class="avaliacao-diagnostica  ">
            <span class="av-title">SOLICITE sua <br><b>avaliação diagnóstica gratuita!</b></span>
            <span class="av-sub">Durante a avaliação identificamos a real necessidade de cada assistido para indicar os cuidados necessários.</span>

            <form id="form-unity" class="form h-100 ">

                <label for="customer_name">
                    <input type="text" id="customer_name" name="customer_name" required placeholder="Nome">
                </label>
                <label for="email">
                    <input type="email" class="email-mask" name="email" placeholder="E-mail" required>
                </label>


                <label for="name">
                    <input type="tel" class="phone" name="phone" placeholder="Telefone" required>

                </label>

                <input type="hidden" name="unity_url" value="<?= get_post_permalink() ?>">

                <input type="hidden" name="unity_email" value="<?= get_field('unit_email') ?>">

                <input type="hidden" name="unity_id" value="<?= get_the_ID() ?>">

                <input type="submit" class="btn-form submit text-white" value="Solicitar">

                <img class="loading d-none" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/loader.svg" alt="">

            </form>


        </div>

    </div>
</section>
<div id="info" class="scroll-header"></div>
<section class="galeria-unidades col-lg-9  px-0 m-auto pt-5 pb-lg-4">

    <div class="d-lg-flex flex-column align-items-center col-lg-7">
        <div class="container-unidades">
            <?php if (have_rows('unit_gallery')) : ?>
                <?php while (have_rows('unit_gallery')) : the_row(); ?>
                    <div class="unidade-item  car-item" style="background: url(<?= the_sub_field('gallery_photo'); ?>)"></div>
                <?php endwhile; ?>
            <?php endif; ?>
        </div>

        <div class="dots-unidades">
            <div class="wrapper-unidade">
                <?php if (have_rows('unit_gallery')) : ?>
                    <?php while (have_rows('unit_gallery')) : the_row(); ?>
                        <button class="thumb-2"><img src="<?= the_sub_field('gallery_photo'); ?>" alt="" class="car-item thumb d-lg-block d-none"></button>
                    <?php endwhile; ?>
                <?php endif; ?>
            </div>
        </div>


    </div>

    <!-- <div class="unidade-control d-lg-flex d-none">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/lefto.svg" alt="">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/righto.svg" alt="">
    </div> -->

    <div class="informacoes col-lg-5 px-xl-3">
        <span class="title d-none d-lg-block"><?= get_field('unit_name') ?></span>
        <span class="address d-none d-lg-block"><b><?= get_field('unit_street') ?></b></span>
        <span class="address d-none d-lg-block"><?= get_field('city_name', get_field('unit_city')) ?> - <?= get_field('states_uf', get_field('unit_state')) ?> - <?= Mask("#####-###", get_field('unit_zipcode')) ?></span>

        <div class="telefones py-3 ">
            <? if(strlen(get_field('unit_phone')) > 0){?>
            <div class="tel d-flex ">
                <p><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/tel.svg" alt=""><?= Mask("## ##", substr(get_field('unit_phone'), 0, 4)) ?><span id="reticencia">...</span><span id="more"><?= Mask("##-####", substr(get_field('unit_phone'), 4, 10)) ?></span></p>
                <button class="myBtn mx-3">Ver mais</button>
            </div>
            <?}?>
            <? if(strlen(get_field('unit_whatsapp')) > 0){?>
            <div class="tel d-flex">
                <p onclick="location.href='https://api.whatsapp.com/send?phone=55<?= get_field('unit_whatsapp'); ?>&text=Ol%C3%A1.%20Gostaria%20de%20saber%20mais%20informa%C3%A7%C3%B5es%20sobre%20a%20Home%20Angels';" style="cursor: pointer;"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/whats.svg" alt=""><?= Mask("## ##", substr(get_field('unit_whatsapp'), 0, 4)) ?><span id="reticencia">...</span><span id="more"><?= Mask("###-####", substr(get_field('unit_whatsapp'), 4, 11)) ?></span></p>
                <button class="myBtn mx-3">Ver mais</button>
            </div>
            <?}?>
        </div>

        <div class="d-none midias d-lg-block">
            <span class="col-9 pl-0 pr-4 pr-lg-5 pb-3">Acompanhe mais de perto nossos cuidados</span>
            <div>
                <? if(strlen(get_field('unit_facebook')) > 0){?>
                <a href="<?= get_field('unit_facebook') ?>"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/face.svg" alt=""></a>
                <?}?>
                <? if(strlen(get_field('unit_youtube')) > 0){?>
                <a href="<?= get_field('unit_youtube') ?>"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/youtube.svg" alt=""></a>
                <?}?>
                <? if(strlen(get_field('unit_instagram')) > 0){?>
                <a href="<?= get_field('unit_instagram') ?>"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/insta.svg" alt=""></a>
                <?}?>
            </div>
        </div>
    </div>
</section>


<?php get_template_part('razoes') ?>
<?php
$location = get_field('unit_map');

if (!empty($location)) {
    echo '<div class="acf-map">
	<div class="marker" data-lat="' . $location['lat'] . '" data-lng="' . $location['lng'] . '"></div>
</div>';
}

?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDpEJQQ8yCmIaqnPdOcbdrZ5EdPz4QjaXA"></script>
<script type="text/javascript">
    (function($) {

        /**
         * initMap
         *
         * Renders a Google Map onto the selected jQuery element
         *
         * @date    22/10/19
         * @since   5.8.6
         *
         * @param   jQuery $el The jQuery element.
         * @return  object The map instance.
         */
        function initMap($el) {

            // Find marker elements within map.
            var $markers = $el.find('.marker');

            // Create gerenic map.
            var mapArgs = {
                zoom: $el.data('zoom') || 16,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map($el[0], mapArgs);

            // Add markers.
            map.markers = [];
            $markers.each(function() {
                initMarker($(this), map);
            });

            // Center map based on markers.
            centerMap(map);

            // Return map instance.
            return map;
        }

        /**
         * initMarker
         *
         * Creates a marker for the given jQuery element and map.
         *
         * @date    22/10/19
         * @since   5.8.6
         *
         * @param   jQuery $el The jQuery element.
         * @param   object The map instance.
         * @return  object The marker instance.
         */
        function initMarker($marker, map) {

            // Get position from marker.
            var lat = $marker.data('lat');
            var lng = $marker.data('lng');
            var latLng = {
                lat: parseFloat(lat),
                lng: parseFloat(lng)
            };

            // Create marker instance.
            var marker = new google.maps.Marker({
                position: latLng,
                map: map
            });

            // Append to reference for later use.
            map.markers.push(marker);

            // If marker contains HTML, add it to an infoWindow.
            if ($marker.html()) {

                // Create info window.
                var infowindow = new google.maps.InfoWindow({
                    content: $marker.html()
                });

                // Show info window when marker is clicked.
                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map, marker);
                });
            }
        }

        /**
         * centerMap
         *
         * Centers the map showing all markers in view.
         *
         * @date    22/10/19
         * @since   5.8.6
         *
         * @param   object The map instance.
         * @return  void
         */
        function centerMap(map) {

            // Create map boundaries from all map markers.
            var bounds = new google.maps.LatLngBounds();
            map.markers.forEach(function(marker) {
                bounds.extend({
                    lat: marker.position.lat(),
                    lng: marker.position.lng()
                });
            });

            // Case: Single marker.
            if (map.markers.length == 1) {
                map.setCenter(bounds.getCenter());

                // Case: Multiple markers.
            } else {
                map.fitBounds(bounds);
            }
        }

        // Render maps on page load.
        $(document).ready(function() {
            $('.acf-map').each(function() {
                var map = initMap($(this));
            });
        });

    })(jQuery);
</script>



<section class="gray-desk py-lg-5">
    <div class="demais-servicos col-lg-7 px-0 m-auto">
        <div class="cuidadores p-4 p-lg-0">
            <div class=" col-12  p-4">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cuidadores.png" alt="">
                <span class="title">Cuidadores de idosos</span>
                <p>Cuidar é essencialmente estar por perto. Você não pode estar sempre ao lado do seu familiar, mas os nossos Cuidadores de Idosos Home Angels podem. </p>
                <p>Tenha um profissional capacitado, profissional e carinhoso para atender às necessidades dele em sua própria residência, sem privá-lo do seu ambiente familiar, seus objetos, sua vizinhança e suas recordações.</p>
            </div>
        </div>

        <div class="demais col-lg-7 text-lg-left  px-5 pr-lg-0  py-4">
            <span class="title px-3 px-lg-0">Demais serviços de Cuidados</span>
            <div class="d-lg-flex justify-content-start">
                <ul class="col-lg-5 pr-lg-5">
                    <li class="item">Cuidadores de Adultos</li>
                    <li class="item">Cuidadores de Crianças</li>
                    <li class="item">Cuidadores para Pós-cirúrgico</li>
                    <li class="item">Cuidadores para Pós-parto</li>
                </ul>

                <ul class="col-lg-5">
                    <li class="item">Acompanhante Terapêutico
                        <span class="sub-item">Dependentes químicos</span>
                        <span class="sub-item">Depressão</span>
                        <span class="sub-item">Transtorno bipolar</span>
                        <span class="sub-item">Pânico e Ansiedade</span>
                        <span class="sub-item border-bot">Fobia social</span>
                    </li>

                    <li class="item">Equipe Multidisciplinar
                        <span class="sub-item">Fisioterapia</span>
                        <span class="sub-item">Fonoaudiologia</span>
                        <span class="sub-item">Nutrição</span>
                        <span class="sub-item">Terapia ocupacional</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>



<div class="depo-unidade ">
    <?php get_template_part('depoimentos') ?>
</div>

<section id="encontre">
    <a href="#" data-toggle="modal" data-target="#exampleModal" class="d-lg-none d-flex encontre">
        Avaliação Diagnóstica Gratuita</a>
</section>




<!-- Modal -->
<div class="modal modal-avaliacao fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <span class="title">solicite sua </br>avaliação diagnóstica</span>
                <span class="gratuita">gratuita</span>

                <p> Durante a avaliação identificamos a real necessidade de cada assistido para indicar os cuidados necessários.</p>

                <form id="form-mobile" class="form h-100 ">

                    <label for="customer_name">
                        <input type="text" id="customer_name" name="customer_name" required placeholder="Nome">
                    </label>
                    <label for="email">
                        <input type="email" class="email-mask" name="email" placeholder="E-mail" required>
                    </label>


                    <label for="name">
                        <input type="tel" class="phone" name="phone" placeholder="Telefone" required>

                    </label>

                    <input type="hidden" name="unity_url" value="<?= get_post_permalink() ?>">

                    <input type="hidden" name="unity_email" value="<?= get_field('unit_email') ?>">

                    <input type="hidden" name="unity_id" value="<?= get_the_ID() ?>">

                    <input type="submit" class="btn-form btn-modal submit text-white" value="Solicitar">

                    <img class="loading d-none" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/loader.svg" alt="">

                </form>
            </div>

        </div>
    </div>
</div>





<?php get_template_part('footer-extra') ?>

<?php get_footer(); ?>