<?php get_header(); ?>

<section class="supervisora">

    <div class="textura ">

        <div class="col-lg-10 px-0 m-auto">
            <a href="#" class="back-button d-lg-none">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/backbutton.svg" alt="">
            </a>

            <img class="selo" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/selo.png" alt="">
            <span class="title">Supervisora</span>
            <span class="sub">Ela se importa o tempo todo.</span>

            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/superv.png" alt="" class="d-none d-lg-block">

            <p>Fiel a nossa tradição de buscar o melhor, a Supervisora Técnica da Home Angels realiza num primeiro momento uma Visita de Avaliação Diagnóstica para levantar as necessidades. Em seguida, ela e sua equipe elaboram um Plano de Atendimento que contempla os detalhes do atendimento que será prestado, o período, a frequência e descreve todos os compromissos assumidos com você e com o seu familiar que requer cuidados. </p>

            <p>Semanalmente, a Supervisora Técnica visita o assistido para se certificar de sua satisfação e bem estar, colher solicitações do familiar responsável e orientar o Cuidador para qualquer correção necessária ou mesmo instrução técnica, já que ajustes podem ser necessários. Sempre visando que seu familiar esteja bem e você fique em paz.</p>

            <ul class="d-none d-lg-block">
                <li><span>•</span>Elabora um Plano de Atendimento Detalhado;</li>
                <li><span>•</span>Realiza Visita Semanal de Satisfação;</li>
                <li><span>•</span>Orienta e avalia o Cuidador.”</li>
            </ul>

            <!-- <img class="img-superv" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/supervisora-espelho.png" alt="">
        </div> -->
        </div>

</section>

<section class="funcoes d-lg-none">

    <div class="funcoes-wrapper">

        <div class="funcoes-container">

            <div class="item">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/funcoes1.png" alt="">
                <span class="title">Realiza Visita Semanal de Satisfação</span>
            </div>

            <div class="item">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/funcoes2.png" alt="">
                <span class="title">Elabora um Plano de Atendimento Detalhado
                </span>
            </div>

            <div class="item">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/funcoes3.png" alt="">
                <span class="title">Orienta e avalia o Cuidador</span>
            </div>

        </div>

    </div>

    <div class="funcoes-nav">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-arrowl.svg" alt="">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-arrowr.svg" alt="">
    </div>

</section>


<?php get_template_part('footer-extra'); ?>

<?php get_template_part('encontre'); ?>

<?php get_footer(); ?>