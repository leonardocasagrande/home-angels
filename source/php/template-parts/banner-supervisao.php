<section class="superv-at">

    <div class="col-lg-9 px-2 m-auto ">
        <span class="titulo d-lg-none col-lg-4">Supervisão Técnica</span>
        <span class="titulo d-none d-lg-block col-lg-5 px-0s">Supervisão</br>Home Angels</span>

        <p>Além do excelente trabalho desempenhado pelo Cuidador de idosos, a Home Angels disponibliza semanalmente a Supervisora Técnica, que garante a qualidade do serviço prestado, dando instruções técnicas ao Cuidador quando necessário e avalicação constante do bem-estar do assisitido.</p>

        <p><b>Conheça mais sobre o trabalho da Supervisora Técnica e a diferença que ela pode fazer no dia a dia de quem você ama.</b></p>
        <a href="<?php echo get_site_url(); ?>/supervisao-tecnica" class="btn-geral">saiba mais</a>
    </div>


</section>