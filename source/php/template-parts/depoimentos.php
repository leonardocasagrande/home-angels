<section class="depoimentos d-block d-lg-none px-4 py-lg-5 pb-5 pt-2">
    <span class="title px-5">Palavra de quem confia!</span>
    <?php
    $args = array(
        'post_type' => 'depoimento',
        'posts_per_page' => -1,
        'tax_query'      => array(
            array(
                'taxonomy' => 'categoria',
                'field'    => 'slug',
                'terms'    => 'franqueado',
                'operator' => 'NOT EXISTS'
            )
        )
    );
    if(is_page('seja-um-franqueado')){
        $args = array(
            'post_type' => 'depoimento',
            'posts_per_page' => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => 'categoria',
                    'field'    => 'slug',
                    'terms'    => 'franqueado',
                ),
            ),
        );
    }     
    $post_query = new WP_Query($args);

    if ($post_query->have_posts()) {
        while ($post_query->have_posts()) {
            $post_query->the_post();
    ?>
            <div class="card pb-3">
                <div class="author">
                    <span><b><?= get_field('nome_do_depoente') ?></b> - <i><?= get_field("local") ?></i></span>
                    <button class="icon closed " type="button" data-toggle="collapse" data-target="#depoimento-<?= get_the_ID() ?>" aria-expanded="false" aria-controls="depoimento-<?= get_the_ID() ?>">
                    </button>
                </div>



                <div class="collapse  content" id="depoimento-<?= get_the_ID() ?>">
                    <p>
                        <?php echo get_field('depoimento') ?>
                    </p>
                </div>
            </div>
    <?php }
    }
    ?>
    </div>
</section>






<section class="d-none d-lg-block depoimentos-desk" id="depoimentos-desktop">
    <div class="textura container-depo ">

        <span class="title">Palavra de quem confia!</span>

        <div class="col-lg-9 mx-auto">
            <div class="card-container ">
                <?php
                $args = array(
                    'post_type' => 'depoimento',
                    'posts_per_page' => -1,
                    'tax_query'      => array(
                        array(
                            'taxonomy' => 'categoria',
                            'field'    => 'slug',
                            'terms'    => 'franqueado',
                            'operator' => 'NOT EXISTS'
                        )
                    )
                    
                );
                if (is_page('seja-um-franqueado')) {
                    $args = array(
                        'post_type' => 'depoimento',
                        'posts_per_page' => -1,
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'categoria',
                                'field'    => 'slug',
                                'terms'    => 'franqueado',
                            ),
                        ),
                    );
                }
                $post_query = new WP_Query($args);

                if ($post_query->have_posts()) {
                    while ($post_query->have_posts()) {
                        $post_query->the_post();
                ?>
                        <div class="item-slider">
                            <div class="card-desk ">
                                <span><b><?= get_field('nome_do_depoente') ?></b><br><i><?= get_field("local") ?></i></span>
                                <div class="line"></div>
                                <div class="destaque">
                                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/aspas.svg" alt="">

                                </div>
                                <p><?php echo get_excerpt(50) ?></p>
                                <p class="collapse" id="depoimentodesk-<?= get_the_ID() ?>"><?php echo get_field('depoimento') ?></p>

                            </div>
                        </div>

                <?php }
                }
                ?>

            </div>
                <a href="#" class="leia-mais btn-fechar d-none">Fechar Depoimentos</a>
        </div>



        <div class="controls col-lg-11 m-auto">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/arrowl.svg" alt="">
            <img src="<?= get_stylesheet_directory_uri() ?>/dist/img/arrowr.svg" alt="">
        </div>

    </div>
</section>