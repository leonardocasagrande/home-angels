<section class="excelencia d-lg-none">

    <span class="title col-8">O cuidado por excelência</span>

    <div class="banner"></div>

    <div class="container-excelencia ">

        <div class="excelencia-wrapper">

            <div class="item">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/exc-2.png" alt="">
                <span class="title">Atendimento </br>24 por 7</span>
                <span class="title">-</span>
                <p>Você conta com planos de cobertura 24 horas por dia, todos os dias da semana, o que garante uma cobertura total do assistido.</p>

            </div>

            <div class="item">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/exc-2.png" alt="">
                <span class="title">Supervisão</span>
                <span class="title">-</span>
                <p>A constante avaliação dos cuidadores, diretamente no lar dos assistidos, é o que garante o padrão de excelência nos nossos atendimentos!</p>

            </div>

            <div class="item">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/exc-2.png" alt="">
                <span class="title">365 dias </br>por ano</span>
                <span class="title">-</span>
                <p>Não importa o dia da semana, ou se é feriado, com a Home Angels, você tem a certeza de ter um profissional qualificado a qualquer dia do ano.</p>

            </div>

            <div class="item">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/exc-2.png" alt="">
                <span class="title">Plano </br>personalizado</span>
                <span class="title">-</span>
                <p>Com nossos cuidadores e a supervisora, o plano de cuidados é personalizado, respeitando as necessidades de cada um de nossos assistidos.</p>

            </div>


            <div class="item">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/exc-2.png" alt="">
                <span class="title">Vantagens </br>exclusivas</span>
                <span class="title">-</span>
                <p>Só a Home Angels possui parcerias exclusivas de cobertura médica e benefícios, levando ainda mais valor e cuidados ao assistido e a família.</p>

            </div>


        </div>
        <div class="btn-geral">saiba mais</div>
    </div>

    <!-- <div class="excelencia-nav d-none ">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrowlb.svg" alt="">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrowrb.svg" alt="">
    </div> -->


    <div class="dots-excelencia pt-5">
        <button></button>
        <button></button>
        <button></button>
        <button></button>
        <button></button>
    </div>

</section>

<section class="excelencia-desk d-none d-lg-block">

    <div class="container col-lg-10  px-0 pt-lg-5">
        <span class="title  col-5 col-xxl-6">O cuidado por excelência</span>

        <div class="tab-content">
            <div id="excelencia1" class="tab-pane show fade in  active">
                <div class="d-flex justify-content-between ">
                    <div class="col-lg-6 col-xxl-6 texto">
                        <span class="tab-title">Atendimento 24 por 7</span>
                        <p>Você conta com planos de cobertura 24 horas por dia, todos os dias da semana, o que garante uma cobertura total do assistido.</p>
                        <a href="#" class="btn-geral">saiba mais</a>
                    </div>

                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/excelencia.png" alt="">
                </div>
            </div>

            <div id="excelencia2" class="tab-pane  fade  ">
                <div class="d-flex justify-content-between ">
                    <div class="col-lg-6 col-xxl-6 texto">
                        <span class="tab-title">Supervisão</span>
                        <p>A constante avaliação dos cuidadores, diretamente no lar dos assistidos, é o que garante o padrão de excelência nos nossos atendimentos!</p>
                        <a href="#" class="btn-geral">saiba mais</a>
                    </div>

                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/supervisao.png" alt="">
                </div>
            </div>

            <div id="excelencia3" class="tab-pane  fade  ">
                <div class="d-flex justify-content-between ">
                    <div class="col-lg-6 col-xxl-6 texto">
                        <span class="tab-title">365 dias por ano</span>
                        <p>Não importa o dia da semana, ou se é feriado, com a Home Angels, você tem a certeza de ter um profissional qualificado a qualquer dia do ano.</p>
                        <a href="#" class="btn-geral">saiba mais</a>
                    </div>

                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/365-por-ano.png" alt="">
                </div>
            </div>

            <div id="excelencia4" class="tab-pane  fade  ">
                <div class="d-flex justify-content-between ">
                    <div class="col-lg-6 col-xxl-6 texto">
                        <span class="tab-title">Plano personalizado</span>
                        <p>Com nossos cuidadores e a supervisora, o plano de cuidados é personalizado, respeitando as necessidades de cada um de nossos assistidos.</p>
                        <a href="#" class="btn-geral">saiba mais</a>
                    </div>

                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/plano-personalisado.png" alt="">
                </div>
            </div>

            <div id="excelencia5" class="tab-pane  fade  ">
                <div class="d-flex justify-content-between ">
                    <div class="col-lg-6 col-xxl-6 texto">
                        <span class="tab-title">Vantagens exclusivas</span>
                        <p>Só a Home Angels possui parcerias exclusivas de cobertura médica e benefícios, levando ainda mais valor e cuidados ao assistido e a família.</p>
                        <a href="#" class="btn-geral">saiba mais</a>
                    </div>

                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/vantagens-exceloencia.png" alt="">
                </div>
            </div>

        </div>


        <ul class="nav nav-pills">
            <li class="">
                <a data-toggle="pill" href="#excelencia1" class="active">
                    <div class="elipse-gray">
                        <div class="pill-item">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/exc-1.png" alt="">
                        </div>
                    </div>
                    <span class="cuidado">ATENDIMENTO 24 POR 7</span>
                </a>
            </li>

            <li>
                <a data-toggle="pill" href="#excelencia2">
                    <div class="elipse-gray">

                        <div class="pill-item">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/exc-2-desk.png" alt="">
                        </div>
                    </div>
                    <span class="cuidado">SUPERVISÃO</span>
                </a>
            </li>

            <li>
                <a data-toggle="pill" href="#excelencia3">
                    <div class="elipse-gray">

                        <div class="pill-item">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/exc-3.png" alt="">
                        </div>
                    </div>
                    <span class="cuidado">365 DIAS </br class="d-none d-lg-block">POR ANO</span>
                </a>
            </li>

            <li>
                <a data-toggle="pill" href="#excelencia4">
                    <div class="elipse-gray">

                        <div class="pill-item">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/exc-4.png" alt="">
                        </div>
                    </div>
                    <span class="cuidado">PLANO PERSONALIZADO</span>
                </a>
            </li>

            <li>
                <a data-toggle="pill" href="#excelencia5">
                    <div class="elipse-gray">

                        <div class="pill-item">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/exc-5.png" alt="">
                        </div>
                    </div>
                    <span class="cuidado">VANTAGENS EXCLUSIVAS</span>
                </a>
            </li>
        </ul>


    </div>
</section>