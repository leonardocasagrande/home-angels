<!-- <img class="d-lg-none d-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/razoes-mobile.png" alt=""> -->

<div class="razoes-mobile d-lg-none">

    <span class="title">Razões para você escolher a Home Angels</span>

    <div class="razoes-wrapper">
        <div class="razoes-container">

            <div class="razao">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/1.png" alt="">
                <span class="title">Avaliação diagnóstica gratuita</span>
            </div>

            <div class="razao">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/2.png" alt="">
                <span class="title">Cuidadores treinados e personalizados</span>
            </div>

            <div class="razao">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/3.png" alt="">
                <span class="title">Supervisão permanente com orientação aos familiares</span>
            </div>

            <div class="razao">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/4.png" alt="">
                <span class="title">Acompanhamento terapêutico</span>
            </div>

            <div class="razao">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/5.png" alt="">
                <span class="title">Planos personalizados</span>
            </div>

            <div class="razao">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/6.png" alt="">
                <span class="title">Maior franquia da América Latina</span>
            </div>

            <div class="razao">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/7.png" alt="">
                <span class="title">Operação nacional</span>
            </div>

            <div class="razao">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/8.png" alt="">
                <span class="title">Relato diário aos familiares</span>
            </div>

            <div class="razao">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/9.png" alt="">
                <span class="title">Equipe de reabilitação multidisciplinar</span>
            </div>

            <div class="razao">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/10.png" alt="">
                <span class="title">Atendimento de alta qualidade</span>
            </div>

        </div>

    </div>

    <div class="razoes-nav" aria-label="Carousel Navigation" tabindex="0">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrowlb.svg" alt="">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrowrb.svg" alt="">
    </div>

</div>
<div class="razoes">
    <img class="w-100" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/razoes-desk.png" alt="">
</div>

<div id="map"></div>