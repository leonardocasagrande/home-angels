<section class="encontre-unidade encontre-servico  ">

    <div class="box  px-4 py-5">
        <span class="title pb-5 px-lg-3">Encontre uma unidade </br>Home Angels <span class="d-none d-lg-inline">perto de você.</span></span>

        <form id="find-unity">
            <div class="row align-items-center justify-content-center">
                <div class="col-md-3">
                    <select name="states" class="select-address" id="states" data-placeholder="Estado" data-next-step="cities">
                        <option value="" disabled selected>Estado</option>

                    </select>

                </div>


                <div class="col-md-3">
                    <select name="cities" id="cities" class="select-address" data-next-step="zones" data-placeholder="Cidade" disabled>
                        <option value="" disabled selected>Cidade</option>

                    </select>

                </div>

                <div class="d-none col-md-3" id="zone-form">

                    <select name="zones" class="select-address " id="zones" data-placeholder="Zona" data-next-step="false" disabled>
                        <option value="" disabled selected>Zona</option>



                    </select>

                </div>



                <div class="col-md-3 text-center">
                    <button type="submit" id="btn-find-unity" class="btn-geral m-1 disabled" disabled> buscar </button>
                </div>



        </form>

    </div>

</section>