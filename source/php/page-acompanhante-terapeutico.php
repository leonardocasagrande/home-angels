<?php get_header(); ?>


<section class="servicos-banner">
    <div class="banner-mobile bg-at">
        <div class="col-lg-8 m-auto px-0 d-flex h-100 align-items-center">
            <span class="banner-title d-none col-lg-5 pl-2 d-lg-block"><?php the_title(); ?></span>
        </div>
    </div>
</section>

<section class="conteudo margin-correction pb-2 mb-2 col-11 col-lg-10  ">

    <a href="#" class="back-button d-lg-none">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/backbutton.svg" alt="">
    </a>

    <div class="d-lg-flex justify-content-between  position-relative">
        <div class="col-lg-6 font-ajust pr-lg-5 px-0">
            <span class="title d-lg-none"><?php the_title(); ?></span>

            <p>O trabalho é indicado para pessoas das mais variadas idades e dificuldades sociais e (ou) emocionais. “A prioridade é o desenvolvimento de comportamentos para a melhoria da qualidade de vida e de interação do paciente, na qual o terapeuta vai até o ambiente dele a partir de uma estratégia definida e planejada”, explica o psicólogo Filipe Colombini, professor do Curso de AT do AMBAN-IPq-HCFMUSP (Ambulatório de Ansiedade do Instituto de Psiquiatria da Universidade de São Paulo (USP).</p>

            <p>O acompanhamento Terapêutico (AT) é uma prática clínica e de cuidados a pessoas em sofrimento psíquico, que necessitam de ajuda circunstancial ou contínua, por vezes em condição de isolamento e com grandes dificuldades de conduzir sua vida. Muito diferente da convencional, busca promover maior organização, propiciando estabelecimento de laços sociais e autonomia da pessoa, geralmente conduzida por profissionais da saúde e educação: psicólogos, enfermeiros, técnicos de enfermagem, pedagogos com formação em AT, supervisionado por um psicólogo mais experiente, podendo ser complementado por outros profissionais da saúde que atendam o caso.</p>

            <div class="orange-box d-lg-none">
                <img class="col-12 px-4 d-lg-none" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/aomp-terapeutica-mobile.png" alt="">
                <span>É um recurso utilizado tanto em estados de crise aguda, como em períodos crônicos de angústia e estagnação.</span>
            </div>

            <p class="margin-discount">A prática tem foco na reabilitação psicossocial e reinserção do indivíduo na sociedade e ambientes sociais, além da reorganização das AVDs (Atividades de vida diárias), a partir de uma escuta qualificada e técnicas terapêuticas, normalmente complementar à terapia. O atendimento acontece nos espaços e lugares de vida das pessoas : domicílios, escolas, ambientes de trabalho, podendo atuar nos mais diversos lugares dentro do contexto de cada paciente. O (AT) irá acompanhar o paciente e pessoas próximas a ele em uma série de situações, tais como saídas de campo e intervenções familiares. A duração e freqüência dos atendimentos, bem como o propósito, são construídos singularmente diante de cada caso.</p>
        </div>

        <div class="col-7 col-xxl-6 col-lg-6 px-0">
            <img class="d-none col-12 px-0 side-img mt-lg-5 d-lg-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/acomp-terapeutico.png" alt="">

            <div class="balao-destaque d-none d-lg-flex">

                <div class="border-custom"></div>
                <div class="box">
                    <span class="text">A HOME ANGELS já sendo excelência em cuidadores de idosos, possui agora Acompanhantes Terapêuticos. Profissional que por um longo período foi chamado de: Amigo Qualificado, Ego Auxiliar entre outros, independente de sua nomenclatura, tem por objetivo ajudar os pacientes críticos, mantendo os princípios éticos da empresa, atendimento humanizado e de qualidade.</span>
                </div>
                <div class="border-custom"></div>

            </div>
        </div>

        <div class="balao-info at-position custom-width d-lg-flex d-none">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/aspas.svg" alt="">
            <p>É um recurso utilizado tanto em estados de crise aguda, como em períodos crônicos de angústia e estagnação.</p>
        </div>

    </div>
</section>

<section class="acompanhamento-terapeutico pt-lg-5">

    <span class="titulo col-lg-7 col-10">Indicações para o trabalho do AT</span>

    <div class="at-wrapper mx-lg-auto">

        <div class="at-container d-lg-none">

            <div class="item">
                <div class="at-item">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/at-1.svg" alt="">
                    <span class="caption">Pessoas com transtornos psíquicos, Depressão, Ataques de Pânico, Transtorno Obsessivo Compulsivo (TOC), Transtorno de Estresse Pós Traumático (TEPT)</span>
                </div>
            </div>

            <div class="item">
                <div class="at-item">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cadroda.svg" alt="">
                    <span class="caption">Pessoas com doenças físicas ou mental ou Idosos com comprometimentos em decorrência do envelhecimento</span>
                </div>
            </div>

            <div class="item">
                <div class="at-item">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/at-3.svg" alt="">
                    <span class="caption">Transtorno do Déficit de Atenção com Hiperatividade (TDAH), Transtorno Afetivo Bipolar (TAB) e demais transtornos de ansiedade e personalidade.</span>
                </div>
            </div>

            <div class="item">
                <div class="at-item h-custom">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/at-4.svg" alt="">
                    <span class="caption">Crianças e adolescentes com dificuldades escolares (emocionais, sensoriais e cognitivas)</span>
                </div>
            </div>

            <div class="item">
                <div class="at-item h-custom">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/at-5.svg" alt="">
                    <span class="caption">Pessoas com fobias ou trauma, crises ou urgências ou dependência química</span>
                </div>
            </div>

        </div>

        <div class="at-container-lg d-none d-lg-flex">

            <div class="item">
                <div class="at-item">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/at-1.svg" alt="">
                    <span class="caption">Pessoas com transtornos psíquicos, Depressão, Ataques de Pânico, Transtorno Obsessivo Compulsivo (TOC), Transtorno de Estresse Pós Traumático (TEPT)</span>
                </div>
            </div>

            <div class="item">
                <div class="at-item">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cadroda.svg" alt="">
                    <span class="caption">Pessoas com doenças físicas ou mental ou Idosos com comprometimentos em decorrência do envelhecimento</span>
                </div>
            </div>

            <div class="item">
                <div class="at-item">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/at-3.svg" alt="">
                    <span class="caption">Transtorno do Déficit de Atenção com Hiperatividade (TDAH), Transtorno Afetivo Bipolar (TAB) e demais transtornos de ansiedade e personalidade.</span>
                </div>
            </div>

            <div class="item">
                <div class="at-item h-custom">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/at-4.svg" alt="">
                    <span class="caption">Crianças e adolescentes com dificuldades escolares (emocionais, sensoriais e cognitivas)</span>
                </div>
            </div>

            <div class="item">
                <div class="at-item h-custom">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/at-5.svg" alt="">
                    <span class="caption">Pessoas com fobias ou trauma, crises ou urgências ou dependência química</span>
                </div>
            </div>

        </div>
    </div>

    <!-- <div class="at-control d-flex d-lg-none justify-content-between">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-arrowl.svg" alt="">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-arrowr.svg" alt="">
    </div> -->


    <div class="balao-destaque d-lg-none">

        <div class="border-custom"></div>
        <div class="box">
            <span class="text">A HOME ANGELS já sendo excelência em cuidadores de idosos, possui agora Acompanhantes Terapêuticos. Profissional que por um longo período foi chamado de: Amigo Qualificado, Ego Auxiliar entre outros, independente de sua nomenclatura, tem por objetivo ajudar os pacientes críticos, mantendo os princípios éticos da empresa, atendimento humanizado e de qualidade.</span>
        </div>
        <div class="border-custom"></div>

    </div>
</section>

<section class="superv-at">

    <div class="col-lg-10 m-auto ">
        <span class="titulo d-lg-none col-lg-4">Supervisão Técnica</span>
        <span class="titulo d-none d-lg-block col-lg-5">Supervisão </br>Home Angels</span>

        <p>Além do excelente trabalho desempenhado pelo Cuidador de idosos, a Home Angels disponibliza semanalmente a Supervisora Técnica, que garante a qualidade do serviço prestado, dando instruções técnicas ao Cuidador quando necessário e avalicação constante do bem-estar do assisitido.</p>

        <p><b>Conheça mais sobre o trabalho da Supervisora Técnica e a diferença que ela pode fazer no dia a dia de quem você ama.</b></p>
        <a href="<?php echo get_site_url(); ?>/supervisao-tecnica" class="btn-geral">saiba mais</a>
    </div>


</section>





<?php get_template_part('avaliacao-gratuita'); ?>
<?php get_template_part('encontre'); ?>

<?php get_footer(); ?>