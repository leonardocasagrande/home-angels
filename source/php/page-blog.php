<?php get_header(); ?>


<section class="banner-blog">
    <div class="col-lg-10 m-auto">
        <div class="orange">
            <div class="box-blue">
                <h2 class="title">Para a Home Angels informar também é cuidar</h2>
            </div>
        </div>
    </div>
</section>


<section class="search-blog pt-4 px-0 col-12">

    <div class="col-lg-10 px-0 pt-lg-5">

        <!-- <div class="search-bar mb-lg-4">
            <input id="searchValue" type="text" placeholder="Pesquisar">
            <a href="#" id="searchButton"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/lupa.svg" alt=""></a>
        </div> -->

        <ul class="nav nav-tabs categorias pb-4 ">
            <li><a class="cat active" data-toggle="tab" href="#menu1">Cuidados com Idosos</a></li>
            <li class="divisor d-none d-lg-flex"></li>
            <li><a class="cat" data-toggle="tab" href="#menu2">Cuidados com Adultos</a></li>
            <li class="divisor d-none d-lg-flex"></li>

            <li><a class="cat" data-toggle="tab" href="#menu3">Cuidados com Crianças</a></li>
            <li class="divisor d-none d-lg-flex"></li>

            <li><a class="cat" data-toggle="tab" href="#menu4">Cuidados Pós-cirúrgico</a></li>
            <li class="divisor d-none d-lg-flex"></li>

            <li><a class="cat" data-toggle="tab" href="#menu5">Cuidados Pós-parto</a></li>
        </ul>
    </div>
</section>


<section class="response-news  pt-lg-5">
    <div class="col-lg-9 col-xxl-8 px-0 m-auto">
        <div class="tab-content  w-100">

            <div id="menu1" class="tab-pane blog-news py-4 pt-lg-0 active show  fade">

                <div class="news-header d-lg-none">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/world.svg" alt="">
                    <h2 class="title">Novidades</h2>
                </div>

                <div class="post-container">

                    <?php
                    $args = array(
                        'post_type' => 'noticia',
                        'posts_per_page' => 1,
                        'category_name' => 'cuidados-com-idosos'
                    );

                    $post_query = new WP_Query($args);

                    if ($post_query->have_posts()) {
                        while ($post_query->have_posts()) {
                            $post_query->the_post();

                    ?>



                            <div class="new-post">

                                <div class="post-thumb" style="background: url(<?= get_field('image'); ?>)"><a href="<?= the_permalink(); ?>"></a></div>
                                <div class=" text">
                                    <span class="titulo"><?= the_title(); ?></span>
                                    <span class="excerpt d-none d-lg-block"><?php the_excerpt(); ?></span>

                                    <p class="d-none d-lg-flex"><?php the_date(); ?></p>

                                    <a href="<?= the_permalink(); ?>" class="btn-geral mb-5 mb-lg-0">ler matéria</a>
                                </div>
                            </div>

                    <?php }
                    }
                    ?>



                </div>


                <a href="#" class="ver-mais ">VEJA <span class="d-none d-lg-inline">MAIS</span><span class="d-inline d-lg-none">+</span><span class="down"></span></a>
            </div>

            <div id="menu2" class="tab-pane blog-news py-4 pt-lg-0   fade">

                <div class="news-header d-lg-none">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/world.svg" alt="">
                    <h2 class="title">Novidades</h2>
                </div>

                <div class="post-container">

                    <?php
                    $args = array(
                        'post_type' => 'noticia',
                        'posts_per_page' => 3,
                        'category_name' => 'cuidados-com-adultos'
                    );

                    $post_query = new WP_Query($args);

                    if ($post_query->have_posts()) {
                        while ($post_query->have_posts()) {
                            $post_query->the_post();
                    ?>



                            <div class="new-post">

                                <div class="post-thumb" style="background: url(<?= get_field('image'); ?>)"></div>
                                <div class=" text">
                                    <h3 class="titulo"><?= the_title(); ?></h3>
                                    <span class="excerpt d-none d-lg-block"><?php the_excerpt(); ?></span>

                                    <p class="d-none d-lg-flex"><?php the_date(); ?></p>

                                    <a href="<?= the_permalink(); ?>" class="btn-geral mb-5 mb-lg-0">ler matéria</a>
                                </div>
                            </div>

                    <?php }
                    }
                    ?>



                </div>

                <!-- <a href="#" class="ver-mais">VEJA MAIS<span class="down"></span></a> -->
            </div>
            <div id="menu3" class="tab-pane blog-news py-4 pt-lg-0   fade">

                <div class="news-header d-lg-none">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/world.svg" alt="">
                    <h2 class="title">Novidades</h2>
                </div>

                <div class="post-container">

                    <?php
                    $args = array(
                        'post_type' => 'noticia',
                        'posts_per_page' => 3,
                        'category_name' => 'cuidados-com-criancas'
                    );

                    $post_query = new WP_Query($args);

                    if ($post_query->have_posts()) {
                        while ($post_query->have_posts()) {
                            $post_query->the_post();
                    ?>



                            <div class="new-post">

                                <div class="post-thumb" style="background: url(<?= get_field('image'); ?>)"></div>
                                <div class=" text">
                                    <span class="titulo"><?= the_title(); ?></span>
                                    <span class="excerpt d-none d-lg-block"><?php the_excerpt(); ?></span>

                                    <p class="d-none d-lg-flex"><?php the_date(); ?></p>

                                    <a href="<?= the_permalink(); ?>" class="btn-geral mb-5 mb-lg-0">ler matéria</a>
                                </div>
                            </div>

                    <?php }
                    } else {
                        echo "<h2 class='title'>Ainda não temos notícias para essa categoria</h2>";
                    }
                    ?>



                </div>

                <!-- <a href="#" class="ver-mais">VEJA MAIS<span class="down"></span></a> -->
            </div>
            <div id="menu4" class="tab-pane blog-news py-4 pt-lg-0   fade">

                <div class="news-header d-lg-none">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/world.svg" alt="">
                    <h2 class="title">Novidades</h2>
                </div>

                <div class="post-container">

                    <?php
                    $args = array(
                        'post_type' => 'noticia',
                        'posts_per_page' => 3,
                        'category_name' => 'cuidados-pos-cirurgico'
                    );

                    $post_query = new WP_Query($args);

                    if ($post_query->have_posts()) {
                        while ($post_query->have_posts()) {
                            $post_query->the_post();
                    ?>



                            <div class="new-post">

                                <div class="post-thumb" style="background: url(<?= get_field('image'); ?>)"></div>
                                <div class=" text">
                                    <span class="titulo"><?= the_title(); ?></span>
                                    <span class="excerpt d-none d-lg-block"><?php the_excerpt(); ?></span>

                                    <p class="d-none d-lg-flex"><?php the_date(); ?></p>

                                    <a href="<?= the_permalink(); ?>" class="btn-geral mb-5 mb-lg-0">ler matéria</a>
                                </div>
                            </div>

                    <?php }
                    } else {
                        echo "<h2 class='title'>Ainda não temos noticias para essa categoria</h2>";
                    }
                    ?>



                </div>

                <!-- <a href="#" class="ver-mais">VEJA MAIS<span class="down"></span></a> -->
            </div>
            <div id="menu5" class="tab-pane blog-news py-4 pt-lg-0   fade">

                <div class="news-header d-lg-none">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/world.svg" alt="">
                    <h2 class="title">Novidades</h2>
                </div>

                <div class="post-container">

                    <?php
                    $args = array(
                        'post_type' => 'noticia',
                        'posts_per_page' => 3,
                        'category_name' => 'cuidados-pos-parto'
                    );

                    $post_query = new WP_Query($args);

                    if ($post_query->have_posts()) {
                        while ($post_query->have_posts()) {
                            $post_query->the_post();
                    ?>



                            <div class="new-post">

                                <div class="post-thumb" style="background: url(<?= get_field('image'); ?>)"></div>
                                <div class=" text">
                                    <span class="titulo"><?= the_title(); ?></span>
                                    <span class="excerpt d-none d-lg-block"><?php the_excerpt(); ?></span>

                                    <p class="d-none d-lg-flex"><?php the_date(); ?></p>

                                    <a href="<?= the_permalink(); ?>" class="btn-geral mb-5 mb-lg-0">ler matéria</a>
                                </div>
                            </div>

                    <?php }
                    }
                    ?>



                </div>

                <!-- <a href="#" class="ver-mais">VEJA MAIS<span class="down"></span></a> -->
            </div>

            <div class="search-response"></div>

        </div>



    </div>
    </div>
</section>



<?php get_template_part('footer-extra'); ?>


<?php get_footer(); ?>