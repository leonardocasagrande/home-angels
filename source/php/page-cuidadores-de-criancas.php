<?php get_header(); ?>


<section class="servicos-banner">
    <div class="banner-mobile bg-criancas">
        <div class="col-lg-8 m-auto px-0 d-flex h-100 align-items-center">
            <span class="banner-title d-none col-lg-4 pl-2 d-lg-block"><?php the_title(); ?></span>
        </div>
    </div>
</section>

<section class="conteudo margin-correction col-11 col-lg-10 ">

    <a href="#" class="back-button d-lg-none">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/backbutton.svg" alt="">
    </a>

    <div class="d-lg-flex justify-content-between position-relative">
        <div class="col-lg-4 px-0">
            <span class="title d-lg-none"><?php the_title(); ?></span>

            <p class="font-size-24"><b>A criança é o bem mais valioso da humanidade</b></p>

            <p>A participação da família é indispensável em qualquer quadro de enfermidade ou deficiência de uma criança. Contudo, a presença de um Cuidador, também chamado de babá, preparado para ajudar a criança e para interagir com a família traz grandes possibilidades de alívio para todos. </p>

            <p>A Home Angels possui Cuidadores e babás treinados para atender crianças em recuperação de saúde, com limitação de mobilidade, com deficiência visual, com síndromes e estados de saúde permanentes. Brincar, vestir, manter a higiene e alimentar-se passam a ser atividades acompanhadas de atenção e carinho. </p>

            <p>O Cuidador não apenas reconhece a família como uma fonte de informação, mas como pessoas que podem ser orientadas e treinadas para cuidar da criança na sua ausência.ador.</p>

            <p>Dessa forma, um grupo integrado de pessoas comprometidas e capacitadas se forma para garantir uma nova realidade a uma criança que só quer ser amada.</p>

        </div>
        <img class="d-none h-50 col-lg-7 col-xxl-6 px-0 side-img d-lg-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/img-criancas.png" alt="">

        <div class="balao-info criancas-position d-lg-flex d-none">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/aspas.svg" alt="">
            <p>Orientação aos pais sobre como cuidar da criança na ausência do cuidador<span class="text-white">”</span></p>
        </div>

    </div>

    <div class="atribuicoes mt-4 mt-lg-0">
        <div class="content">
            <span class="title">Atribuições de<br> um Cuidador de Crianças</span>

            <div class="d-lg-flex col-lg-10 justify-content-center px-0 m-auto">
                <div class="d-flex  align-items-center flex-lg-column col-lg-6  text-center ">
                    <div class="external-circle">
                        <div class="circle">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/crianca.png" alt="">
                        </div>
                    </div>
                    <span class="col-8 col-lg-12 titulo">Atendimento a Síndromes com apoio<br> ao planejamento terapêutico.</span>
                </div>

                <div class="divisor d-lg-none"></div>

                <div class="d-flex  align-items-center flex-lg-column text-center col-lg-6">
                    <div class="external-circle">
                        <div class="circle">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/coracao-mao.png" alt="">
                        </div>
                    </div>
                    <span class="col-8 col-lg-12 titulo">Cuidados Especiais e companhia<br> carinhosa
                        no dia-a-dia.</span>
                </div>

            </div>
        </div>
    </div>
</section>

<?php get_template_part('depoimentos'); ?>

<?php get_template_part('excelencia'); ?>

<?php get_template_part('encontre-servico'); ?>

<?php get_template_part('encontre'); ?>

<?php get_template_part('footer-extra'); ?>

<?php get_footer(); ?>