<?php
//Template Name: unidades

get_header(); ?>
<section class="banner-unidades">

  <div class="banner-1 d-none d-lg-block bg-sobre">
    <div class="filter-white blue"><span class="d-none d-lg-block"></span></div>
  </div>


  <div class="textura unidades-form-mobile" id="selects-params">
    <div class="col-10 col-lg-4 pt-lg-5 margin-center">
      <span class="hello">Olá,!</span>
      <span class="escolha">Agora, basta escolher a unidade mais próxima de você!</span>
    </div>
  </div>


  <div class="container">
    <div class="py-4 unidade px-4">
      <form id="find-unity">
        <div class="row align-items-center justify-content-center">
          <div class="col-md-3">
            <select name="states" class="select-address" id="states" data-placeholder="Estado" data-next-step="cities">
              <option value="" disabled="disabled" selected="selected">Estado</option>
            </select>
          </div>
          <div class="col-md-3">
            <select name="cities" id="cities" class="select-address" data-next-step="zones" data-placeholder="Cidade" disabled="disabled">
              <option value="" disabled="disabled" selected="selected">Cidade</option>
            </select>
          </div>
          <div class="col-md-3 d-none" id="zone-form">
            <select name="zones" class="select-address" id="zones" data-placeholder="Zona" disabled="disabled">
              <option value="" disabled="disabled" selected="selected">Zona</option>
            </select>
          </div>
          <div class="col-md-3 btn-un-form-mob text-align-left"><button type="submit" id="btn-find-unity" class="btn-geral btn-size m-1 disabled" disabled="disabled">buscar</button></div>
        </div>
      </form>
    </div>
  </div>
  <div class="unidade-intermediaria">
    <div id="unidade-container" class="unidade-container col-lg-7 col-12"></div>
  </div>
</section>
<?php get_template_part('footer-extra'); ?>
<?php get_footer(); ?>
<script>
  let customer = JSON.stringify(sessionStorage.getItem("customer"));

  const queryStringToObject = (queryString) => {
    let obj = {}
    if (queryString) {
      queryString.slice(1).split('&').map((item) => {
        const [k, v] = item.split('=')
        v ? obj[k] = v : null
      })
    }
    return obj
  }


  customer = queryStringToObject(decodeURIComponent(customer));




  $(".hello").html(`Ola ${customer.customer_name}`);

  handleUnitsForm();
</script>