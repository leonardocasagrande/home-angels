<?php get_header(); ?>


<section class="trabalhe-conosco">


    <section class="banner-contato d-none d-lg-block">

        <div class="col-lg-8 m-auto ">
            <span class="d-none d-lg-block">Trabalhe </br>Conosco</span>
        </div>

    </section>

    <div class="banner-work">

        <div class="textura">
            <span class="title">Trabalhe na maior rede de </br>cuidadores de idosos do Brasil! </span>

            <span class="sub d-lg-block d-none">Escolha uma especialidade</span>


            <div class="box-form ">
                <div class=" square-detail">
                    <div class="item-input work-1">
                        <label>
                            <span class="title">Seja um cuidador</span>
                            <input type="radio" value="cuidador" checked="checked" name="especialidade">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>

                <div class=" square-detail">
                    <div class="item-input work-2">
                        <label>
                            <span class="title">Equipe Multidisciplinar</span>
                            <input type="radio" value="equipe" name="especialidade">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>

                <div class=" square-detail custom-position">
                    <div class="item-input work-3">
                        <label>
                            <span class="title">Acompanhante Terapêutico</span>
                            <input type="radio" value="acompanhante" name="especialidade">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                </div>
            </div>


            <div class="triangulo">
                <div class="textura"></div>
            </div>
        </div>
    </div>
    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/supervisora.png" alt="">
</section>

<section class="trabalhe-content">

    <span class="title">Preencha seu currículo e venha fazer parte da equipe!</span>

    <div class="trabalhe-form col-lg-7">


        <form action="">
            <div class="custom-grid">
                <input required="true" name="nome-completo-work" class="mr-lg-1" type="text" placeholder="Nome Completo">
                <input required="true" name="data-nasc-work" class="inp-1 data-nasc" type="text" placeholder="Data de nasc.">
            </div>

            <div class="custom-grid">
                <input required="true" name="cep-work" class="inp-2 cep" type="text" placeholder="CEP">
                <select required="true" class="inp-1 mx-lg-1" name="estado-work" id="">
                    <option value="" selected disabled>Estado</option>
                    <option value="AC">Acre</option>
                    <option value="AL">Alagoas</option>
                    <option value="AP">Amapá</option>
                    <option value="AM">Amazonas</option>
                    <option value="BA">Bahia</option>
                    <option value="CE">Ceará</option>
                    <option value="DF">Distrito Federal</option>
                    <option value="ES">Espírito Santo</option>
                    <option value="GO">Goiás</option>
                    <option value="MA">Maranhão</option>
                    <option value="MT">Mato Grosso</option>
                    <option value="MS">Mato Grosso do Sul</option>
                    <option value="MG">Minas Gerais</option>
                    <option value="PA">Pará</option>
                    <option value="PB">Paraíba</option>
                    <option value="PR">Paraná</option>
                    <option value="PE">Pernambuco</option>
                    <option value="PI">Piauí</option>
                    <option value="RJ">Rio de Janeiro</option>
                    <option value="RN">Rio Grande do Norte</option>
                    <option value="RS">Rio Grande do Sul</option>
                    <option value="RO">Rondônia</option>
                    <option value="RR">Roraima</option>
                    <option value="SC">Santa Catarina</option>
                    <option value="SP">São Paulo</option>
                    <option value="SE">Sergipe</option>
                    <option value="TO">Tocantins</option>
                </select>
                <input required="true" name="cidade-work" class="inp-2" type="text" placeholder="Cidade">
            </div>

            <div class="custom-grid">
                <input required="true" name="endereco-work" type="text" placeholder="Endereço">
                <input required="true" name="numero-work" class="inp-2 mx-lg-1" type="text" placeholder="Número">
                <input required="true" name="comp-work" class="inp-1" type="text" placeholder="Complemento">
                <input required="true" name="bairro-work" type="text" class="ml-lg-1" placeholder="Bairro">
            </div>


            <div class="custom-grid ">
                <input required="true" name="telefone-work" class="inp-1 phone" type="text" placeholder="Telefone (__)">
                <input required="true" name="celular-work" class="mx-lg-1 phone" type="text" placeholder="Celular">
                <input required="true" class="operadora-work" class="inp-2" type="text" placeholder="Operadora">
            </div>

            <input required="true" name="email-work" type="text" placeholder="E-mail" onblur="validateEmail(this);">


            <textarea required="true" name="formacao-work" id="" cols="30" rows="10" placeholder="Formação Escolar"></textarea>

            <textarea required="true" name="habilidades-work" id="" cols="30" rows="10" placeholder="Habilidades"></textarea>

            <textarea required="true" name="comentarios-work" id="" cols="30" rows="10" placeholder="Comentários"></textarea>

            <span class="pergunta">Como ficou sabendo da franquia Home Angels?</span>
            <select required="true" name="pergunta-work" id="" class="inp-pergunta">
                <option value="Google">Google</option>
                <option value="Portal ABF">Portal ABF</option>
                <option value="Redes Sociais">Redes Sociais</option>
                <option value="Revista">Revista</option>
                <option value="Indicação">Indicação</option>
                <option value="Facebook">Facebook</option>
                <option value="TV">TV</option>
                <option value="Jornal">Jornal</option>
                <option value="Guias">Guias</option>
                <option value="Visitas a Feiras">Visitas a Feiras</option>
                <option value="E-mail">E-mail</option>
                <option value="Outros">Outros</option>
            </select>

            <img class="m-auto" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/captcha.png" alt="">

            <input type="submit" value="enviar" name="submit-work" class="btn-geral">
        </form>
    </div>


</section>


<?php get_template_part('footer-extra'); ?>


<?php get_footer(); ?>