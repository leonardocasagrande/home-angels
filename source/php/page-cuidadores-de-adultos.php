<?php get_header(); ?>


<section class="servicos-banner">
    <div class="banner-mobile bg-adultos">
        <div class="col-lg-8 m-auto px-0 d-flex h-100 align-items-center">
            <span class="banner-title d-none col-lg-4 pl-2 d-lg-block"><?php the_title(); ?></span>
        </div>
    </div>
</section>

<section class="conteudo  col-11 col-lg-10 ">

    <a href="#" class="back-button d-lg-none">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/backbutton.svg" alt="">
    </a>

    <div class="d-lg-flex justify-content-between position-relative">
        <div class="col-lg-6 pr-lg-5 px-0">
            <span class="title d-lg-none"><?php the_title(); ?></span>

            <p class="font-size-24"><b>Adultos, no auge de suas vidas, podem precisar de ajuda</b></p>

            <p>As atividades do cotidiano dependem da capacidade motora. Alimentar-se, fazer higiene, vestir-se, trabalhar, caminhar, estudar e ir ao médico são desafios a vencer. </p>

            <p>A vida adulta é generosa em oportunidades e realizações. Toda limitação física pode ser amenizada quando uma ajuda dedicada é oferecida e promove a continuidade profissional, a recuperação da saúde ou mesmo a adaptação para uma nova realidade física. </p>

            <p>Adultos com mobilidade reduzida por um histórico de Acidente Vascular Cerebral, acidentes automobilísticos, deficiências, doença de Alzheimer, Parkinson, entre outras, experimentam um grande alívio com a presença dedicada de um Cuidador.</p>

            <p>A Home Angels possui Cuidadores preparados para atender adultos que apresentam déficits motores. São profissionais treinados em técnicas específicas para realizar atividades e ajudar na recuperação do estado de saúde, ou mesmo nas rotinas profissionais.</p>

        </div>
        <img class="d-none  h-50 col-lg-6 col-xxl-6 px-0 side-img d-lg-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/adultos.png" alt="">

        <div class="balao-info adultos-position d-lg-flex d-none">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/aspas.svg" alt="">
            <p>Tarefas simples, como caminhar ou vestir-se, podem se tornar um desafio<span class="text-white">”</span></p>
        </div>

    </div>

    <div class="atribuicoes mt-4 mt-lg-0">
        <div class="content">
            <span class="title">Atribuições de<br> um Cuidador de Adultos</span>

            <div class="d-lg-flex col-lg-10 px-0  m-auto">
                <div class="d-flex align-items-center px-0 flex-lg-column col-lg-4 text-center ">
                    <div class="external-circle">
                        <div class="circle">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/higiene.png" alt="">
                        </div>
                    </div>
                    <span class="col-9 col-lg-12 pr-0 titulo">Cuidados básicos de higiene, alimentação e vestimenta.</span>
                </div>

                <div class="divisor d-lg-none"></div>

                <div class="d-flex align-items-center px-0 flex-lg-column text-center col-lg-4">
                    <div class="external-circle">
                        <div class="circle">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cama.png" alt="">
                        </div>
                    </div>
                    <span class="col-8 col-lg-8 titulo">Cuidados </br class="d-none d-lg-flex">pós-cirúrgicos.</span>
                </div>

                <div class="divisor d-lg-none"></div>

                <div class="d-flex align-items-center px-0 flex-lg-column text-center col-lg-4">
                    <div class="external-circle">
                        <div class="circle">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/muleta.png" alt="">
                        </div>
                    </div>
                    <span class="col-8 col-lg-8 titulo">Auxílio na </br class="d-none d-lg-block">adaptação física.</span>
                </div>

            </div>
        </div>
    </div>
</section>

<?php get_template_part('depoimentos'); ?>

<?php get_template_part('excelencia'); ?>

<?php get_template_part('encontre-servico'); ?>


<?php get_template_part('encontre'); ?>

<?php get_template_part('footer-extra'); ?>

<?php get_footer(); ?>