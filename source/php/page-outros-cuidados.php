<?php get_header() ?>

<section class="servicos-banner ">
    <div class="banner-mobile ">
        <div class="col-lg-8 m-auto px-0 d-flex h-100 align-items-center">
            <span class="banner-title d-none col-lg-4 pl-2 d-lg-block"><?php the_title(); ?></span>
        </div>
    </div>
</section>

<section class="servicos-intro px-0 py-lg-5 d-lg-flex justify-content-between col-lg-10 col-xxl-8 m-auto">

    <div class="cuidadores col-11 col-lg-5 my-lg-5 ">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/home1.png" alt="" class="d-lg-block d-none col-12 pb-3 px-0">
        <span class="title pb-3">Cuidadores de Idosos</span>
        <p class="pb-2">Cuidar é essencialmente estar por perto. Você não pode estar sempre ao lado do seu familiar, mas os nossos Cuidadores de Idosos Home Angels podem. </p>
        <p>Tenha um profissional capacitado, profissional e carinhoso para atender às necessidades dele em sua própria residência, sem privá-lo do seu ambiente familiar, seus objetos, sua vizinhança e suas recordações.</p>

        <a href="<?php echo get_site_url(); ?>/cuidadores-de-idosos" class="btn-geral">conheça o serviço</a>

    </div>


    <div class="col-lg-6 px-0 pt-lg-5">
        <span class="title px-lg-0">Demais serviços de Cuidados</span>

        <div class="demais-servicos-2 d-lg-flex pt-4 pb-1">

            <div class="col-8 col-lg-6 m-auto pb-4">
                <a href="<?= get_site_url() ?>/cuidadores-de-adultos" class="btn-servico d-flex justify-content-between">
                    <span class="col-9   px-0  d-block">Cuidadores de Adultos</span>
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/show.svg" alt="">
                </a>

                <a href="<?= get_site_url() ?>/cuidadores-de-criancas" class="btn-servico d-flex justify-content-between">
                    <span class="col-9   px-0  d-block">Cuidadores de Crianças</span>
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/show.svg" alt="">
                </a>

                <a href="<?= get_site_url() ?>/cuidadores-para-pos-cirurgico" class="btn-servico d-flex justify-content-between">
                    <span class="col-9 col-lg-9 col-xxl-8 px-0  d-block">Cuidadores para Pós-cirúrgico</span>
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/show.svg" alt="">
                </a>

                <a href="<?= get_site_url() ?>/cuidadores-para-pos-parto" class="btn-servico d-flex justify-content-between">
                    <span class="col-9   px-0  d-block">Cuidadores para Pós-parto</span>
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/show.svg" alt="">
                </a>
            </div>

            <div class="d-lg-block col-lg-6 d-none m-auto pb-4">

                <a href="<?= get_site_url() ?>/acompanhante-terapeutico" class="btn-servico d-none border-none d-lg-block ">
                    <div class="d-flex justify-content-between">
                        <span class="col-9 col-lg-8 px-0  d-block">Acompanhante Terapêutico</span>
                        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/show.svg" alt="">
                    </div>
                </a>
                <span class="sub d-block">Dependentes químicos</span>
                <span class="sub d-block">Depressão</span>
                <span class="sub d-block">Transtorno bipolar</span>
                <span class="sub d-block">Pânico e Ansiedade</span>
                <span class="sub d-block b-bottom">Fobia social</span>

                <a href="<?= get_site_url() ?>/equipe-multidisciplinar" class="btn-servico d-none border-none  d-lg-block ">
                    <div class="d-flex justify-content-between">
                        <span class="col-9 col-lg-8 px-0  d-block">Equipe Multidisciplinar</span>
                        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/show.svg" alt="">
                    </div>
                </a>
                <span class="sub d-block">Fisioterapia</span>
                <span class="sub d-block">Fonoaudiologia</span>
                <span class="sub d-block">Nutrição</span>
                <span class="sub d-block">Terapia Ocupacional</span>
            </div>



            <div class="servicos-especiais d-lg-none">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/acompanhanteterapeutico.png" alt="">
                <div class="col-11 m-auto bg-orange">
                    <span class="title">Acompanhante Terapêutico</span>

                    <p>O Acompanhante Terapêutico-AT Home Angels atua como apoio complementar ao médico psiquiatra e ao terapeuta psicólogo, seguindo suas orientações e zelando pela reestruturação dos hábitos e a reintegração individual e familiar do assistido.</p>

                    <a href="<?php echo get_site_url(); ?>/acompanhante-terapeutico" class="btn-geral bg-blue">saiba mais</a>
                </div>

                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/multidisciplinar.png" alt="">
                <div class="col-11 m-auto bg-blue">
                    <span class="title pr-5">Equipe Multidisciplinar</span>

                    <p>A Franquia Home Angels dispõe de equipe multidisciplinar com Fisioterapeuta, Fonoaudiólogo, Terapeuta Ocupacional e Nutricionista para a recuperação da saúde de quem você ama.</p>

                    <a href="<?= get_site_url(); ?>/equipe-multidisciplinar" class="btn-geral bg-orange">saiba mais</a>
                </div>

            </div>
        </div>
    </div>

</section>

<?php get_template_part('avaliacao-gratuita') ?>

<?php get_template_part('excelencia') ?>


<?php get_template_part('encontre') ?>

<?php get_template_part('banner-supervisao'); ?>


<?php get_template_part('footer-extra') ?>




<?php get_footer() ?>