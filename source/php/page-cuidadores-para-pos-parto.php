<?php get_header(); ?>


<section class="servicos-banner">
    <div class="banner-mobile bg-pos-parto">
        <div class="col-lg-8 m-auto px-0 d-flex h-100 align-items-center">
            <span class="banner-title d-none col-lg-6 pl-2 d-lg-block"><?php the_title(); ?></span>
        </div>
    </div>
</section>

<section class="conteudo  col-11 col-lg-10 ">


    <a href="#" class="back-button d-lg-none">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/backbutton.svg" alt="">
    </a>

    <div class="d-lg-flex justify-content-between  position-relative">
        <div class="col-lg-6 px-0 pr-lg-4">
            <span class="title d-lg-none"><?php the_title(); ?></span>

            <p class="font-size-24"><b>O cuidado especial que esse momento<br> merece e requer</b></p>

            <p>O período de cuidados de uma gestante pode ser necessário desde os primeiros meses da gravidez. Após o parto, os cuidados essenciais duram de 6 a 8 semanas e só terminam com o retorno do ciclo. No caso de mães de múltiplos, os cuidados são ainda maiores.</p>

            <p>Os dias na maternidade e o retorno ao consultório do obstetra geralmente requerem ajuda que o Cuidador está apto a prestar. As mães precisam ser bem cuidadas e a posição de amamentação corrigida para reduzir dores e fissuras. O acúmulo de leite excessivo também precisa ser acompanhado, assim como a dieta prescrita pelo médico.</p>

            <p>Desafio. A maternidade é uma dádiva e o desejo de ter e cuidar de um bebê é universal entre as mulheres. Mas, e quando eles vêm em dobro (ou mais)? </p>

            <p>O desafio de criar múltiplos é, sem dúvida, um dos maiores pelos quais uma mulher pode passar. Simplesmente porque, é óbvio, tudo se multiplica: o cansaço, o vestir, a alimentação, a higiene, etc. Nesses casos, os Cuidadores da Home Angels fazem uma grande diferença.</p>

            <p>A Home Angels possui Cuidadores treinados para auxiliar as mães no delicado período de pós-parto. Ele presta auxílio na alimentação, no vestir e na higiene, bem como no acompanhamento às consultas médicas de rotina e apoio emocional em caso de depressão pós-parto. Também ajuda nos cuidados inerentes à saúde e bem-estar dos recém-nascidos</p>



        </div>
        <img class="d-none col-lg-6 col-xxl-6 px-0  h-25 d-lg-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pos-parto.png" alt="">

        <div class="balao-info nenem-position d-lg-flex d-none">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/aspas.svg" alt="">
            <p>Ajuda nos cuidados com a mãe, em especial na higiene, alimentação e vestimenta”</p>
        </div>

    </div>

    <div class="atribuicoes">
        <div class="content">
            <span class="title">Atribuições de<br> um Cuidador de Crianças</span>

            <div class="d-lg-flex col-lg-10 justify-content-center px-0 m-auto">
                <div class="d-flex px-0 align-items-center flex-lg-column col-lg-4 text-center ">
                    <div class="external-circle">
                        <div class="circle">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/mamae.png" alt="">
                        </div>
                    </div>
                    <span class="col-8 col-lg-11 titulo">Acompanhamento às consultas médicas de rotina.</span>
                </div>

                <div class="divisor d-lg-none"></div>

                <div class="d-flex px-0 align-items-center flex-lg-column text-center col-lg-4">
                    <div class="external-circle">
                        <div class="circle">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/higiene.png" alt="">
                        </div>
                    </div>
                    <span class="col-8 col-lg-11 titulo">Auxílio na higiene,<br> vestimenta e na alimentação.</span>
                </div>

                <div class="divisor d-lg-none"></div>

                <div class="d-flex px-0 align-items-center flex-lg-column text-center col-lg-4">
                    <div class="external-circle">
                        <div class="circle">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/handshake.png" alt="">
                        </div>
                    </div>
                    <span class="col-8 col-lg-11 titulo">Apoio emocional em caso de depressão pós-parto.</span>
                </div>

            </div>
        </div>
    </div>
</section>

<?php get_template_part('depoimentos'); ?>

<?php get_template_part('excelencia'); ?>

<?php get_template_part('encontre-servico'); ?>

<?php get_template_part('encontre'); ?>

<?php get_template_part('footer-extra'); ?>

<?php get_footer(); ?>