<?php get_header(); ?>

<section class="intro-home ">
    <div class="carosel-banners">
        <div class="banner-help">
        <a href="<?= get_site_url(); ?>/home-angels-help" class="link-banner"></a>
        
        </div>
        <div class="banner-home">

            <div class="col-lg-10 m-auto h-100 d-none d-lg-block">
                <div class="box-intro orange px-0 col-lg-4">
                    <div class="box-intro blue">
                        <span class="title">Um familiar idoso merece todo nosso cuidado e atenção</span>
                    </div>
                </div>
            </div>

            

        </div>
    </div>
    <div class="covid-btn">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/covid.png" alt="">
                <a href=" <?= get_site_url(); ?>/covid-19">
                    <span class="text">Saiba o protocolo que seguimos.</span>
                    <i class="fa fa-arrow-right" aria-hidden="true"></i></a>
            </div>
    <div class="box col-11 col-lg-9  p-3">
        <div class="col-lg-6  px-0 pl-lg-0 pl-xxl-5">
            <span class="top-info d-lg-block d-none">nós cuidamos de quem você ama</span>

            <span class="title col-7 col-lg-12 px-lg-0 pb-0">Cuidadores de Idosos</span>

            <span class="title d-lg-none p-0">-</span>

            <span class="sub px-4 pb-2 d-lg-none">Cuidar é essencialmente estar por perto. </span>

            <p class="d-lg-none">Você não pode estar sempre ao lado do seu familiar, mas os nossos Cuidadores de Idosos Home Angels podem. </p>

            <p class="p-lg d-none d-lg-block">"O cuidado especial que o idoso merece, sem privá-lo do ambiente familiar e da sua rotina.”</p>
        </div>
        <img class="d-none d-lg-flex" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/home1.png" alt="" class="d-none-d-lg-flex">
        <a class="btn-geral ml-xxl-5" href="<?= get_site_url(); ?>/cuidadores-de-idosos">conheça o serviço</a>
    </div>

    <a href="<?= get_site_url(); ?>/outros-cuidados" class="btn-geral d-none d-lg-flex ">DEMAIS SERVIÇOS DE CUIDADOS</a>


</section>

<section class="encontre-unidade  bg-lg-transparent px-lg-4 py-5">

    <div class="box col-11 px-4 py-5">
        <span class="title pb-5 px-lg-3">encontre a unidade mais próxima</span>


        <form id="find-unity">
            <select name="states" class="select-address" id="states" data-placeholder="Estado" data-next-step="cities">
                <option value="" disabled selected>Estado</option>

            </select>

            <select name="cities" id="cities" class="select-address" data-next-step="zones" data-placeholder="Cidade" disabled>
                <option value="" disabled selected>Cidade</option>

            </select>

            <div class="d-none" id="zone-form">

                <select name="zones" class="select-address " id="zones" data-placeholder="Zona" data-next-step="false" disabled>
                    <option value="" disabled selected>Zona</option>



                </select>


            </div>

            <button type="submit" id="btn-find-unity" class="btn-geral mt-4 disabled" disabled> buscar </button>

        </form>
    </div>

</section>


<?php get_template_part('depoimentos'); ?>

<?php get_template_part('avaliacao-gratuita'); ?>

<?php get_template_part('excelencia'); ?>


<section class="gama-completa d-none d-lg-block">
    <div class="textura">

        <div class=" col-lg-10 m-auto h-100 align-items-center d-flex justify-content-between">
            <div class="text">
                <span class="bigger">Oferecemos uma gama completa </span>
                <span>de serviços para cuidar de quem você ama!</span>
            </div>

            <a href="<?= get_site_url(); ?>/outros-cuidados" class="btn-geral m-0">DEMAIS SERVIÇOS DE CUIDADOS</a>
        </div>

    </div>
</section>



<?php get_template_part('razoes'); ?>

<?php get_template_part('vantagens'); ?>

<section class="blog-home py-5 pb-lg-0 px-3">

    <span class="title "><b class="detail">Informar</b> também é cuidar</span>

    <div class="blog-home-container">
        <div class="blog-home-wrapper">
            <?php
            $args = array(
                'post_type' => 'noticia',
                'posts_per_page' => 5,
                'category__not_in' => array(6)

            );

            $post_query = new WP_Query($args);

            if ($post_query->have_posts()) {
                while ($post_query->have_posts()) {
                    $post_query->the_post();
            ?>
                    <div class="item">

                        <div class="img-bg" style="background-image:url(<?= get_field('image'); ?>)"></div>


                        <h3 class="titulo"><?= the_title(); ?></h3>

                        <!-- <p class="px-3 d-lg-none"><?= get_the_excerpt(); ?></p> -->

                        <a href="<?= the_permalink(); ?>">Leia +</a>
                    </div>


            <?php }
            }
            ?>


        </div>
    </div>

    <div class="blog-home-nav">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrowl.svg" alt="">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/arrowr.svg" alt="">
    </div>

    <div class="blog-home-dots d-lg-none">
        <button></button>
        <button></button>
        <button></button>
        <button></button>
        <button></button>

    </div>

    <a href="<?= get_site_url(); ?>/blog" class="btn-geral">Ver todos</a>

</section>

<section class="certificacoes-home">

    <span class="title d-lg-none">A única franquia com Certificações ABF</span>

</section>

<?php get_template_part('encontre'); ?>

<?php get_footer(); ?>