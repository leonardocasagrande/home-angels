<?php get_header(); ?>


<section class="servicos-banner">
    <div class="banner-mobile bg-pos-cirurgico">
        <div class="col-lg-8 m-auto px-0 d-flex h-100 align-items-center">
            <span class="banner-title d-none col-lg-5 pl-2 d-lg-block"><?php the_title(); ?></span>
        </div>
    </div>
</section>

<section class="conteudo  col-11 col-lg-10 ">

    <a href="#" class="back-button d-lg-none">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/backbutton.svg" alt="">
    </a>

    <div class="d-lg-flex justify-content-between position-relative">
        <div class="col-lg-5 pr-lg-0 pl-0 pr-4">
            <span class="title d-lg-none"><?php the_title(); ?></span>

            <p class="font-size-24"><b>O cuidado adequado após a cirurgia<br> faz toda diferença</b></p>

            <p>Após qualquer intervenção cirúrgica, o corpo humano precisa de tempo para se regenerar. Muitas alterações ocorrem nos primeiros dias após a cirurgia e para se recuperar o organismo precisa de toda ajuda possível. Repouso, alimentação adequada e no horário, banho e auxílio para evitar esforço fazem a diferença.</p>

            <p>A Home Angels possui cuidadores treinados para acompanhar pessoas desde a internação até a alta hospitalar, com continuidade do atendimento na residência. Eles são orientados a observar e registrar qualquer alteração que possa surgir, como inchaços, sangramentos no local do corte, vermelhidão, dores, etc. E assim oferecer as informações essenciais ao acompanhamento médico. </p>

            <p>Além de prestar cuidados básicos de higiene, alimentação e vestimenta, o Cuidador também faz o acompanhamento a terapias orientadas, aos retornos médicos e no controle dos medicamentos. </p>



        </div>
        <img class="d-none col-lg-6 col-xxl-6 px-0 h-25 d-lg-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/pos-cirurgico.png" alt="">

        <div class="balao-info adultos-position d-lg-flex d-none">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/aspas.svg" alt="">
            <p>Acompanhamento da internação à alta hospitalar e também no pós-cirúrgico<span class="text-white">”</span></p>
        </div>
    </div>
    <div class="atribuicoes">
        <div class="content">
            <span class="title">Atribuições de<br> um Cuidador de Crianças</span>

            <div class="d-lg-flex col-lg-12 justify-content-center px-0 m-auto">
                <div class="d-flex px-0 align-items-center flex-lg-column col-lg-4 text-center ">
                    <div class="external-circle">
                        <div class="circle">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/higiene.png" alt="">
                        </div>
                    </div>
                    <span class="col-8 col-lg-12 titulo">Cuidados básicos de higiene, </br class="d-none d-lg-block">alimentação e vestimenta.</span>
                </div>

                <div class="divisor d-lg-none"></div>

                <div class="d-flex px-0 align-items-center flex-lg-column text-center col-lg-4">
                    <div class="external-circle">
                        <div class="circle">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cama.png" alt="">
                        </div>
                    </div>
                    <span class="col-8 col-lg-8 titulo">Acompanhamento a terapias orientadas.</span>
                </div>

                <div class="divisor d-lg-none"></div>

                <div class="d-flex px-0 align-items-center flex-lg-column text-center col-lg-4">
                    <div class="external-circle">
                        <div class="circle">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/remedio.png" alt="">
                        </div>
                    </div>
                    <span class="col-8 col-lg-8 titulo">Controle dos medicamentos.</span>
                </div>

            </div>
        </div>
    </div>
</section>

<?php get_template_part('depoimentos'); ?>

<?php get_template_part('excelencia'); ?>

<?php get_template_part('encontre-servico'); ?>

<?php get_template_part('encontre'); ?>

<?php get_template_part('footer-extra'); ?>

<?php get_footer(); ?>