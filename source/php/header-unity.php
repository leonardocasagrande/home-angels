<!DOCTYPE html>

<html lang="pt_BR">

<head>

  <meta charset="UTF-8">

  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <title>

    <?php wp_title(''); ?>

  </title>

  <meta name="robots" content="index, follow" />

  <meta name="msapplication-TileColor" content="#ffffff">

  <meta name="theme-color" content="#ffffff">

  <?php wp_head(); ?>

  <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" />


</head>

<body>

  <noscript id="deferred-styles"></noscript>


  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/tiny-slider/2.9.2/tiny-slider.css">

  <link rel="stylesheet" type="text/css" href="<?= get_stylesheet_directory_uri(); ?>/dist/css/style.css">

  </noscript>

  <div class="pre-header d-none d-lg-block">
    <div class="d-none d-lg-flex col-9 justify-content-end">
      <a href="<?= get_site_url() ?>/seja-um-franqueado"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/franq.svg" alt="">Seja um franqueado</a>
      <a href="<?= get_site_url() ?>/trabalhe-conosco"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/balao.svg" alt="">Trabalhe conosco</a>
      <a href="http://zaiom.com.br/franqueado/login.asp"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/engrenagem.svg" alt="">Área do franqueado</a>
      <!-- <a href="#"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/star.svg" alt="">Portal do cliente</a> -->
    </div>
  </div>


  <header>
    <nav class="col-lg-10 justify-content-between">

      <div id="mySidenav" class="sidenav d-lg-none">
        <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
        <a href="<?= get_site_url() ?>/sobre-nos">Nossa História</a>
        <a href="<?= get_site_url() ?>/cuidadores-de-idosos">Cuidadores de Idosos</a>
        <a href="<?= get_site_url() ?>/outros-cuidados">Outros Cuidados</a>
        <a href="<?= get_site_url() ?>/nossas-unidades">Nossas Unidades</a>
        <a href="<?= get_site_url() ?>/blog">Blog</a>
        <a href="<?= get_site_url() ?>/vantagens">Vantagens Exclusivas</a>
        <a href="<?= get_site_url() ?>/trabalhe-conosco">Trabalhe Conosco</a>
        <a href="<?= get_site_url(); ?>/contato">Contato</a>
        <a href="<?= get_site_url() ?>/seja-um-franqueado">Seja um Franqueado</a>
        <!-- <a href="#"><img class="pr-2" src="<?= get_stylesheet_directory_uri() ?>/dist/img/star.svg">Portal do cliente</a> -->
        <div>
          <a href="https://www.facebook.com/homeangelsbr"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/face.svg"></a>
          <a href="https://www.instagram.com/homeangelsbrasil/"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/insta.svg"></a>
          <a href="https://www.youtube.com/user/Homeangelsbr"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/youtube.svg"></a>
        </div>
      </div>



      <a style="margin: 0 auto;" href="<?= get_site_url() ?>/">
        <img class="logo-home" src="<?= get_stylesheet_directory_uri() ?>/dist/img/logo.png">
      </a>

      <div class="d-none unity-header desk-links d-lg-flex">
        <a id="" href="#info"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/unidade.svg" alt="">Unidade</a>
        <a id="" href="#map"><img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/como-chegar.svg" alt="">Como chegar</a>
      </div>

      <span class="d-lg-none" onclick="openNav()">&#9776;</span>
    </nav>


  </header>