<?php get_header(); ?>

<section class="covid-page">
    <div class="desk-banner">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/covid.png" alt="">
    </div>

    <div class="box-desk pt-lg-4">
        <div class="box col-lg-8 pr-lg-0 ">
            <a class="back-button d-lg-none voltar" href="#"><img src="<?= get_stylesheet_directory_uri() ?>/dist/img/voltar.svg" alt="voltar"></a>
            <h1 class="title d-lg-none">COVID-19</h1>

            <p>Esta página foi desenvolvida para compartilhar com nossos clientes e suas famílias, informações relacionadas ao Coronavirus e sua doença, a COVID-19. Vamos postar histórias, dicas e orientações oficiais do Governo, as sim como do Departamento Técnico da Home Angels Brasil. Nossa intenção é mantê-los informados com o que há de mais importante no momento atual.
            </p>
        </div>

        <div class="textura col-lg-8 m-auto video">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/3QstSOnvf6U" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>

<?php get_template_part('avaliacao-gratuita'); ?>

<section class="d-lg-none principais-sintomas">

    <div class="card col-lg-7 pb-3 py-lg-2">
        <div class="sintoma">
            <span class="pergunta">Principais Sintomas</span>
            <button class="icon closed " type="button" data-toggle="collapse" data-target="#pergunta-1" aria-expanded="false" aria-controls="pergunta-1">
            </button>
        </div>

        <div class="collapse  content" id="pergunta-1">
            <p>
                · Febre<br>
                · Tosse seca<br>
                · Cansaço<br>
                · Dores e desconfortos ao respirar<br>
                · Dor de garganta<br>
                · Dor de cabeça<br>
                · Perda de paladar ou olfato<br>

                <br>Procure atendimento médico imediato se tiver sintomas graves. Sempre ligue antes de ir ao médico ou posto de saúde, clínicas ou hospitais.

            </p>
        </div>
    </div>

    <div class="card col-lg-7 pb-3 py-lg-2">
        <div class="sintoma">
            <span class="pergunta">Forma de Transmissão</span>
            <button class="icon closed " type="button" data-toggle="collapse" data-target="#pergunta-2" aria-expanded="false" aria-controls="pergunta-2">
            </button>
        </div>

        <div class="collapse  content" id="pergunta-2">
            <p>
                · Evite aglomerações;<br>
                · Lavar as mãos frequentemente com água e sabão e, na ausência desses, higienizá-las utilizando álcool gel em 70%;<br>
                · Utilize máscaras sempre que precisar sair de casa;<br>
                · Ao tossir e espirrar, cubra o nariz e a boca utilizando um lenço, que deve ser descartado em seguida, ou então a parte interna do cotovelo;<br>
                · Evite cumprimentar com apertos de mão, beijos e abraços;<br>
                · Evite tocar nos olhos, boca e nariz com as mãos sem a devida higienização;<br>
                · Não compartilhe objetos de uso pessoal, como copos e talheres;<br>

            </p>
        </div>
    </div>

    <div class="card col-lg-7 pb-3 py-lg-2">
        <div class="sintoma">
            <span class="pergunta">Medidas Preventivas</span>
            <button class="icon closed " type="button" data-toggle="collapse" data-target="#pergunta-3" aria-expanded="false" aria-controls="pergunta-3">
            </button>
        </div>

        <div class="collapse  content" id="pergunta-3">
            <p>
                A principal forma de transmissão da COVID-19 é por contato com o doente, que, ao tossir ou espirrar, elimina gotículas respiratórias, que acabam contaminando outras pessoas, além de contaminar objetos. Uma pessoa sadia, ao tocar um objeto contaminado e levar a mão à boca, nariz ou olhos, sem antes higienizá-las, pode também se contaminar.
            </p>
        </div>
    </div>



</section>

<section class="d-none d-lg-block sintomas-desk">

    <div class="container">
        <div class="item">
            <span class="title">Pincipal Sintoma</span>
            <div class="divisor"></div>
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/sintoma1.png" alt="">
            <span class="sub">Febre alta</span>
        </div>

        <div class="item">
            <span class="title">Forma de Transmissão mais Comum</span>
            <div class="divisor"></div>
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/sintoma2.png" alt="">
            <span class="sub">Tosse e espirro</span>
        </div>

        <div class="item">
            <span class="title">Melhor Medida Preventiva</span>
            <div class="divisor"></div>
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/sintoma3.png" alt="">
            <span class="sub">Use máscaras e EPIs</span>
        </div>
    </div>

    <span class="content">COVID-19 Atualização</span>
    <span class="text">Leia as informações mais importantes relacionadas ao Coronavíruse à sua doença, a COVID-19 e saiba como agir para se manter seguro e também seus familiares idosos.</span>

    <div class="triangulo"></div>

</section>


<section class="artigos">
    <span class="title">Artigos sobre COVID-19</span>

    <div class="artigos-wrapper">

        <div class="artigos-container">

            <?php
            $args = array(
                'post_type' => 'noticia',
                'posts_per_page' => 3,
                'category_name' => 'covid'
            );

            $post_query = new WP_Query($args);

            if ($post_query->have_posts()) {
                while ($post_query->have_posts()) {
                    $post_query->the_post();
            ?>
                    <div class="artigo">
                        <div class="thumb">
                            <img src="<?= get_field('image') ?>" alt="">
                        </div>
                        <span class="title"><?= get_the_title() ?></span>

                        <a href="<?= get_the_permalink() ?>" class="read-more">leia +</a>
                    </div>

            <?php }
            }
            ?>

        </div>
    </div>
    <div class="artigos-nav">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-arrowl.svg" alt="">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-arrowr.svg" alt="">
    </div>


</section>


<?php get_template_part('encontre'); ?>
<?php get_footer(); ?>