<?php get_header() ?>
<div class="header-mask"></div>
	<div class="cabecalho-help text-center">
		<img src="<?= get_stylesheet_directory_uri() ?>/dist/img/help-logo.png" alt="Help Home Angels" class="img-fluid">
	</div>
	<section class="informacoes-help">
		<div class="fundo-cinza">
			<div class="container">
			<p class="text-left">Agora todos os assistidos da rede Home Angels tem orientação médica por telefone, 24 horas por dia,  7 dias por semana, sem custo adicional ao valor do contrato.</p>
			<img src="<?= get_stylesheet_directory_uri() ?>/dist/img/help-mulher.png" alt="Mulher pedindo ajuda pelo telefone" class="img-fluid bordado">
			</div>
		</div>
		<div class="container">
		<h2 class="gold-title">O Atendimento</h2>
		<p>
		O assistido Home Angels, seus familiares e cuidadoras poderão solicitar a orientação médica para qualquer necessidade de saúde. Eles falarão diretamente com um dos médicos de plantão, que irá esclarecer dúvidas, orientar em situações de saúde e dar instruções para ocorrências de urgências e emergências. 
		</p>
		<h2 class="gold-title">Quando Usar</h2>
		<ul>
			<li>Sempre que houver dúvida sobre alguma doença;</li>
			<li>Para aconselhamento de medicação, sobre dosagens e reações adversas;</li>
			<li>Para tirar dúvidas de natureza geriátrica;</li>
			<li>Em caso de mal-estar, se deve ou não procurar o Pronto Antendimento;</li>
			<li>Para esclarecer dúvidas sobre especialidades médicas;</li>
			<li>Para informações sobre doenças crônicas (diabetes, hipertensão, etc);</li>
			<li>Em situações como: intoxicações, quedas, traumas e queimaduras.</li>
		</ul>
		</div>
	</section>
	<?php get_template_part('avaliacao-gratuita'); ?>
	<hr class="golden">
	<?php get_template_part('encontre'); ?>
<?php get_footer() ?>