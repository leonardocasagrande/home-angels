<?php get_header(); ?>


<section class="servicos-banner">
    <div class="banner-mobile bg-idosos">
        <div class="col-lg-8 m-auto px-0 d-flex h-100 align-items-center">
            <span class="banner-title d-none col-lg-4 pl-2 d-lg-block"><?php the_title(); ?></span>
        </div>
    </div>
</section>

<section class="conteudo col-11 col-lg-10 ">

    <a href="#" class="back-button d-lg-none">
        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/backbutton.svg" alt="">
    </a>

    <div class="d-lg-flex justify-content-between position-relative">
        <div class="col-lg-5 px-0">
            <span class="title d-lg-none"><?php the_title(); ?></span>

            <p class="font-size-24"><b>Um familiar idoso merece todo nosso cuidado e atenção</b></p>

            <p>A Home Angels acredita na importância do convívio familiar como auxiliar na recuperação de quadros de saúde de idosos. Assim, ao invés de colocar seu familiar numa casa de repouso, você pode ter um profissional capacitado para atender às necessidades dele em sua própria residência, sem privá-lo do ambiente familiar e da sua rotina diária. </p>

            <p>Desse modo, o Cuidador faz para o idoso apenas o que o mesmo não consegue realizar sozinho e incentiva atividades adequadas à sua capacidade atual, mas também está junto quando a limitação é grande, preservando a dignidade do assistido. </p>

            <p>Nossos Cuidadores, além da formação técnica, são treinados para cuidar de idosos como eles precisam, promovendo sua independência e auxiliando-os na realização das atividades pré-estabelecidas, visando preservar as funções existentes e estimular as deficitárias. </p>
        </div>
        <img class="d-none  col-lg-6 col-xxl-6 h-75 px-0 side-img d-lg-block" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/idosos.png" alt="">

        <div class="balao-info idosos-position d-lg-flex d-none">
            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/aspas.svg" alt="">
            <p>Incentivo a realizar atividades dentro do limite da capacidade do idoso<span class="text-white">”</span></p>
        </div>

    </div>

    <div class="atribuicoes">
        <div class="content">
            <span class="title">Atribuições de<br> um Cuidador de Idosos</span>

            <div class="d-lg-flex col-lg-10 m-auto">
                <div class="d-flex align-items-center flex-lg-column col-lg-4 text-center ">
                    <div class="external-circle">
                        <div class="circle">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/companhia.png" alt="">
                        </div>
                    </div>
                    <span class="col-8 col-lg-12 titulo">Companhia no Lar, Consultório e Hospital.</span>
                </div>

                <div class="divisor d-lg-none"></div>

                <div class="d-flex align-items-center flex-lg-column text-center col-lg-4">
                    <div class="external-circle">
                        <div class="circle">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/mobilidade.png" alt="">
                        </div>
                    </div>
                    <span class="col-8 col-lg-12 titulo">Cuidados com a mobilidade, banho, higiene, e alimentação.</span>
                </div>

                <div class="divisor d-lg-none"></div>

                <div class="d-flex align-items-center flex-lg-column text-center col-lg-4">
                    <div class="external-circle">
                        <div class="circle">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cerebro.png" alt="">
                        </div>
                    </div>
                    <span class="col-10 col-lg-12 titulo">Cuidados especiais para idosos com Alzheimer, Parkinson e outras demências.</span>
                </div>

            </div>
        </div>
    </div>
</section>

<?php get_template_part('depoimentos'); ?>

<?php get_template_part('excelencia'); ?>


<?php get_template_part('encontre-servico'); ?>

<?php get_template_part('encontre'); ?>

<?php get_template_part('footer-extra'); ?>

<?php get_footer(); ?>