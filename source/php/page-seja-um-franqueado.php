<?php get_header('franqueado'); ?>

<div class="banner-1 bg-sobre d-none  d-lg-block">
    <div class="filter-white">
        <span class="d-none title d-lg-block">Seja um franqueado </span>
    </div>
</div>

<section class="franqueado-intro mb-3 pb-4">
    <span class="title">Venha fazer parte do nosso time de franqueados de sucesso!</span>

    <div class="reverse">
        <p>A Home Angels é a maior franquia de cuidadores de idosos da América Latina, possui mais de 160 franquias em todo o Brasil, tem sido chancelada pelo selo de excelência pela Associação Brasileira de Franchising desde 2014 e é premiada como TOP 25 do Franchising Brasileiro na categoria inovação.</p>

        <div id="questoes" class=" text-center  pb-4">
            <div class="embed-responsive embed-responsive-16by9">
                <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/atsVZYOtdQU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>

<section class="d-block d-lg-none">
    <div id="accordion">
        <div class="card">
            <div class="card-header como-funciona" id="headingOne">
                <button class="btn btn-link" data-toggle="collapse" data-target="#como-funciona" aria-expanded="true" aria-controls="como-funciona">
                    Como funciona
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/show.svg" alt="">
                </button>
            </div>

            <div id="como-funciona" class="collapse " aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                    <p>O processo de envelhecimento da população brasileira constitui um dos maiores fenômenos de mercado dos últimos tempos.</p>
                    <p>Após estudos em diversos países e contatos aprofundados com algumas das principais instituições de saúde no país, desenvolvemos e formatamos um modelo de negócio de Cuidadores de Pessoas adaptado às necessidades da população da terceira idade, de pessoas em recuperação cirúrgica, com necessidades especiais e mães-novas.</p>
                </div>
            </div>
        </div>


        <div class="card">
            <div class="card-header imenso-mercado" id="headingTwo">
                <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#imenso-mercado" aria-expanded="false" aria-controls="imenso-mercado">
                    Imenso mercado
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/show.svg" alt="">

                </button>
            </div>
            <div id="imenso-mercado" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card-body">
                    <p><b>Até 2020 o Brasil terá mais de 31 milhões de idosos com mais de
                            60 anos</b> de idade e é para esse mercado em franco crescimento que direcionamos nossos esforços comerciais e de marketing de forma a consolidar o nosso pioneirismo na área de cuidadores de pessoas no país.
                    </p>

                    <p>Em 15 anos no Brasil, o numero de pessoas idosas subiu 34%, atingindo quase 18% de toda população brasileira, ou mais de 32 milhões de pessoas. Some-se a isso as mais de 5 milhões de pessoas por ano, sem considerar partos e cesarianas, que passam por uma cirurgia e precisam de alguma ajuda no período de convalescença</p>

                    <div class="text-center">
                        <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/grafico.png" alt="">
                    </div>
                </div>
            </div>
        </div>


        <div class="card ">
            <div class="card-header  quem-e-o-cliente " id="headingThree">
                <button class="btn btn-link  collapsed" data-toggle="collapse" data-target="#quem-e-o-cliente" aria-expanded="false" aria-controls="quem-e-o-cliente">
                    Quem é o cliente?
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/show.svg" alt="">

                </button>
            </div>
            <div id="quem-e-o-cliente" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                <div class="card-body pt-0 text-center">
                    São famílias das classes A e B.

                    <img class="pt-2" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/tabela-1.png" alt="">

                    <img class="pt-4" src="<?= get_stylesheet_directory_uri(); ?>/dist/img/tabela-2.png" alt="">
                </div>
            </div>
        </div>


        <div class="card ">
            <div class="card-header  quem-e-franqueado " id="headingFour">
                <button class="btn btn-link  collapsed" data-toggle="collapse" data-target="#quem-e-franqueado" aria-expanded="false" aria-controls="quem-e-franqueado">
                    Quem pode ser nosso franqueado?
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/show.svg" alt="">

                </button>
            </div>
            <div id="quem-e-franqueado" class="collapse " aria-labelledby="headingFour" data-parent="#accordion">
                <div class="card-body ">
                    <p>Se você é qualificado e se identifica com o mundo da prestação serviços de Cuidadores de pessoas, você poderá ser um franqueado Home Angels em sua cidade, pois será treinado para lidar com todas as questões que envolvem o desenvolvimento de um negócio na área de atendimento e serviços de Cuidadores de pessoas.</p>

                    <p>Se você é um médico, enfermeira, psicólogo, terapeuta ocupacional, fisioterapeuta ou técnico em enfermagem encontrará na Home Angels sua oportunidade de fazer um upgrade em sua carreira ao aliar à sua formação, novos conhecimentos que lhe permitirão atuar sinergicamente com sua atividade. Se você é um empreendedor, receberá treinamentos necessários para gerir todos os assuntos relacionados ao negócio.</p>

                    <div class="franqueado-container">
                        <div class="wrapper">
                            <div class="franqueado-item">
                                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/franqueado-1.png" alt="">

                                <span class="name">Rafael Brito</span>
                                <span class="info">Enfermeiro</span>
                                <span class="info">Criciúma - Centro</span>
                            </div>

                            <div class="franqueado-item">
                                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/franqueado-2.png" alt="">

                                <span class="name">Silvia Camila</span>
                                <span class="info">Terapeuta Ocupacional</span>
                                <span class="info">Valinhos - Centro</span>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="unidade-control d-flex justify-content-between">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/lefto.svg" alt="">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/righto.svg" alt="">
                </div>

            </div>
        </div>
    </div>
</section>


<section id="cliente" class="d-none d-lg-block  sobre-desk col-lg-10 pb-5 m-auto">
    <ul class="nav nav-pills  pb-lg-5 m-auto col-lg-10 d-flex justify-content-between mb-3" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">Como Funciona</a>
        </li>

        <div class="divisor"></div>

        <li class="nav-item">
            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Imenso Mercado</a>
        </li>

        <div class="divisor"></div>


        <li class="nav-item">
            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="false">Para quem</a>
        </li>
    </ul>
    <div class="tab-content pt-lg-5" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
            <div class="tab-container">
                <div class="texto pr-lg-3">
                    <span>O processo de envelhecimento da população brasileira constitui um dos maiores fenômenos de mercado dos últimos tempos.</span>

                    <p>Após estudos em diversos países e contatos aprofundados com algumas das principais instituições de saúde no país, desenvolvemos e formatamos um modelo de negócio de Cuidadores de Pessoas adaptado às necessidades da população da terceira idade, de pessoas em recuperação cirúrgica, com necessidades especiais e mães-novas.</p>
                </div>

                <div class="image">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/como-funciona-desk.png" alt="">
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
            <div class="tab-container">

                <div class="image">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/grafico.png" alt="">
                </div>
                <div class="texto">
                    <span>Até 2020 o Brasil terá mais de 31 milhões de idosos com mais de
                        60 anos de idade e é para esse mercado em franco crescimento que direcionamos nossos esforços comerciais e de marketing de forma a consolidar o nosso pioneirismo na área de cuidadores de pessoas no país.
                    </span>

                    <p>Em 15 anos no Brasil, o numero de pessoas idosas subiu 34%, atingindo quase 18% de toda população brasileira, ou mais de 32 milhões de pessoas. Some-se a isso as mais de 5 milhões de pessoas por ano, sem considerar partos e cesarianas, que passam por uma cirurgia e precisam de alguma ajuda no período de convalescença</p>
                </div>

            </div>

        </div>

        <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
            <div class="tab-container">

                <div class="texto">
                    <span>
                        • Idosos com limitações físicas e/ou necessitando companhia;<br>
                        • Pessoas em recuperação cirúrgica;<br>
                        • Vítimas de acidentes;<br>
                        • Pessoas portadoras de Alzheimer e/ou Parkinson;<br>
                        • Mulheres grávidas e mães novas;<br>
                        • Recém-nascidos;<br>
                    </span>
                </div>

                <div class="image">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/teste.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="cliente-desk d-none d-lg-block">
    <div class="textura py-5">
        <div class="col-lg-10 m-auto ">

            <span class="title w-100">Quem é o cliente ?</span>
            <span class="sub-title w-100">São famílias das classes A e B</span>

            <div class="content-container">
                <div class="text">
                    <span class="title pb-3">PLANOS DE ATENDIMENTO</span>

                    <span class="title">VIP</span>
                    <span class="sub-title">Plano de atendimento individual e personalizado</span>

                    <span class="title">GRUPO</span>
                    <span class="sub-title">Plano de atendimento a grupos de pacientes num mesmo horário e local.</span>
                </div>

                <div class="images">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cliente-desk-1.png" alt="">

                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/cliente-desk-2.png" alt="">
                </div>

                <div class="text">
                    <span class="title pb-3">MODELO COMERCIAL</span>

                    <span class="title">FLEX</span>
                    <span class="sub-title">Os cuidadores podem ser contratados diretamente pelo cliente.</span>

                    <span class="title">FULL</span>
                    <span class="sub-title">Os cuidadores são funcionários da franquia.</span>
                </div>

            </div>
        </div>
    </div>
</section>

<section class="franqueado-desk d-none d-lg-block py-5 ">
    <div class="col-lg-10 m-auto d-flex align-items-center justify-content-between">
        <div class="text col-lg-5">
            <span class="title">QUEM PODE SER NOSSO FRANQUEADO?</span>

            <p>Se você é qualificado e se identifica com o mundo da prestação serviços de Cuidadores de pessoas, você poderá ser um franqueado Home Angels em sua cidade, pois será treinado para lidar com todas as questões que envolvem o desenvolvimento de um negócio na área de atendimento e serviços de Cuidadores de pessoas.</p>

            <p>Se você é um médico, enfermeira, psicólogo, terapeuta ocupacional, fisioterapeuta ou técnico em enfermagem encontrará na Home Angels sua oportunidade de fazer um upgrade em sua carreira ao aliar à sua formação, novos conhecimentos que lhe permitirão atuar sinergicamente com sua atividade. Se você é um empreendedor, receberá treinamentos necessários para gerir todos os assuntos relacionados ao negócio.</p>
        </div>

        <div class="franqueado-desk-container   pt-lg-5 ">
            <div class="franq-wrapper d-block">
                <?php
                if (have_rows('franqueados')) :
                    while (have_rows('franqueados')) : the_row();
                ?>
                        <div class="franqueado-item  text-lg-center">
                            <img src="<?= get_sub_field('foto') ?>" alt="<?= get_sub_field('nome') ?>">

                            <span class="name d-lg-block"><?= get_sub_field('nome') ?></span>
                            <span class="info d-lg-block"><?= get_sub_field('cargo') ?></span>
                            <span class="info d-lg-block"><?= get_sub_field('unidade') ?></span>
                        </div>
                <?php endwhile;
                endif; ?>


            </div>

            <div class="unidade-control-desk d-flex justify-content-between">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-arrowl.svg" alt="">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-arrowr.svg" alt="">
            </div>

            <div class="dots-franq-desk">
                <button></button>
                <button></button>
            </div>
        </div>
    </div>
    <div id="diferenciais"></div>
</section>


<section class="diferenciais-franq ">
    <div class="filtro-blue py-lg-5">
        <div class="banner-dif">
            <span>Diferenciais</span>
            <span>Home Angels</span>
        </div>

        <div class="diferenciais-carousel">

            <div class="dif-container   ">
                <div class="dif-wrapper">
                    <div class="item">
                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/alta-lucratividade.png" alt="">
                            <div>
                                <span>ALTA LUCRATIVIDADE</span>
                                <span>-</span>
                                <p>Retorno do investimento com prazo médio acima de 16 meses</p>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/home-office.png" alt="">
                            <div>
                                <span>HOME OFFICE</span>
                                <span>-</span>
                                <p>Início das operações pode ser em sua casa, evitando custos iniciais de locação e reforma de imóvel</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">

                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/franquia-premiada.png" alt="">
                            <div>
                                <span>FRANQUIA PREMIADA</span>
                                <span>-</span>
                                <p>Premiada TOP 25 em Inovação nas Franquias Brasileiras e chancelada pelo selo de excelência no Franchising pela ABF</p>
                            </div>
                        </div>
                    </div>

                    <div class="item">

                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/suporte-operacional.png" alt="">
                            <div>
                                <span>SUPORTE OPERACIONAL</span>
                                <span>-</span>
                                <p>Conte com nossa equipe de suporte especializada para auxiliá-lo no dia a dia da sua unidade</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">

                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/sistema-personalizado.png" alt="">
                            <div>
                                <span>SISTEMA PERSONALIZADO</span>
                                <span>-</span>
                                <p>Um software totalmente desenvolvido especialmente para o negócio Home Angels, possibilitando o eficaz gerenciamento da unidade</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">

                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/mercado-crescente.png" alt="">
                            <div>
                                <span>MERCADO CRESCENTE</span>
                                <span>-</span>
                                <p>O processo de envelhecimento da população brasileira constitui um dos maiores fenômenos de mercado de todos os tempos</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">

                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/negocio-tendencia-mundial.png" alt="">
                            <div>
                                <span>NEGÓCIO DE TENDÊNCIA MUNDIAL</span>
                                <span>-</span>
                                <p>O Mercado de Cuidadores de Idosos vem se tornando tendência internacional, com o envelhecimento da população mundial</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">

                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/baixo-investimento.png" alt="">
                            <div>
                                <span>BAIXO INVESTIMENTO</span>
                                <span>-</span>
                                <p>A possibilidade do inicio Home Office faz com que a Home Angels esteja entre as franquias mais baratas do Brasil</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">

                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/otimos-ganhos.png" alt="">
                            <div>
                                <span>ÓTIMOS GANHOS</span>
                                <span>-</span>
                                <p>A Franquia Home Angels possui um faturamento médio de R$ 80 mil por unidade, devido ao alto ticket médio, e com lucro em média de 25%</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">

                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/prestacao-servicos.png" alt="">
                            <div>
                                <span>PRESTAÇÃO DE SERVIÇOS</span>
                                <span>-</span>
                                <p>Sem o estoque de produtos e alto custo de compra em materiais, o investimento inicial impacta positivamente em sua operação</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">

                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-1.png" alt="">
                            <div>
                                <span>MARCA COM PROJEÇÃO NACIONAL</span>
                                <span>-</span>
                                <p>A Home Angels conta com a experiência de franqueados em todo o território nacional, com mais de 160 franqueados no Brasil</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">

                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-2.png" alt="">
                            <div>
                                <span>OPERAÇÃO PADRONIZADA</span>
                                <span>-</span>
                                <p>Os franqueados contam com o suporte especializado e com o acompanhamento técnico da equipe Home Angels</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">

                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-3.png" alt="">
                            <div>
                                <span>MANUAL DE OPERAÇÃO</span>
                                <span>-</span>
                                <p>Material de apoio às operações para a implantação de sua unidade e orientações obtidas em nossa experiência ao longo dos anos</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">

                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-4.png" alt="">
                            <div>
                                <span>MATERIAIS ADMINISTRATIVOS E PUBLICITÁRIOS</span>
                                <span>-</span>
                                <p>Banco de marketing e materiais de apoio administrativos e técnicos exclusivos, ajudando assim na implantação e operação da unidade</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">

                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/treinamento-completo.png" alt="">
                            <div>
                                <span>TREINAMENTO COMPLETO</span>
                                <span>-</span>
                                <p>Você será preparado para implantar sua unidade Home Office ou em Ponto Comercial pela equipe de suporte Home Angels</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">

                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/metodologia.png" alt="">
                            <div>
                                <span>METODOLOGIA HOME ANGELS</span>
                                <span>-</span>
                                <p>Serviço com qualidade e confiança em toda a rede, devido ao padrão de excelência da franquia Home Angels</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">

                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/site-proprio.png" alt="">
                            <div>
                                <span>SITE PRÓPRIO</span>
                                <span>-</span>
                                <p>Sua unidade terá um site próprio permitindo assim campanhas de marketing digital específicas para sua região</p>
                            </div>
                        </div>
                    </div>
                    <div class="item">

                        <div class="dif">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/fundo-de-propaganda.png" alt="">
                            <div>
                                <span>FUNDO DE PROPAGANDA NACIONAL</span>
                                <span>-</span>
                                <p>Propagandas nacionais com o objetivo de fortalecimento da marca em todo o território nacional</p>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
            <div id="cases"></div>

            <div class="dif-nav">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-arrowl.svg" alt="">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-arrowr.svg" alt="">
            </div>



        </div>
    </div>

</section>


<section class="cases-sucesso col-lg-10 m-auto py-lg-5 d-lg-block d-none">

    <span class="title text-center pb-lg-5">Cases de Sucesso</span>

    <ul class="nav nav-pills col-lg-8 m-auto d-flex justify-content-around mb-3" id="pills-tab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="pills-videos-tab" data-toggle="pill" href="#pills-videos" role="tab" aria-controls="pills-videos" aria-selected="true">Vídeos</a>
        </li>

        <div class="divisor"></div>

        <li class="nav-item">
            <a class="nav-link" id="pills-depos-tab" data-toggle="pill" href="#pills-depos" role="tab" aria-controls="pills-depos" aria-selected="false">depoimentos</a>
        </li>
    </ul>

    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-videos" role="tabpanel" aria-labelledby="pills-videos-tab">
            <div class="tab-container pt-lg-5">

                <div id="investimento"></div>

                <div class="video-container">
                    <?php
                    if (have_rows('videos')) :
                        while (have_rows('videos')) : the_row();
                    ?>
                            <div class="video-item">
                                <?php the_sub_field('link_do_youtube') ?>
                                <span class="video-caption d-block"><?= get_sub_field('legenda') ?></span>
                            </div>
                    <?php endwhile;
                    endif; ?>

                </div>


                <div class="video-nav">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-arrowl.svg" alt="">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-arrowr.svg" alt="">
                </div>

                <!-- <div class="video-dots">
                    <button></button>
                    <button></button>

                </div> -->
            </div>
        </div>

        <div class="tab-pane fade" id="pills-depos" role="tabpanel" aria-labelledby="pills-depos-tab">
            <div class="tab-container">

                <!-- FALTA DEPOIMENTO -->
                <?php get_template_part('depoimentos') ?>
            </div>

        </div>


    </div>

</section>


<section class="investimento ">
    <div class="textura py-5 px-2">

        <span class="title px-1">Investimento de adesão da sua franquia</span>

        <span class="taxa col-lg-7 mx-auto mb-4 ">Taxa de franquia por número de habitantes </span>
        <div id="perguntas"></div>

        <div class="container-faixa col-lg-12 px-0 m-auto py-lg-5 d-lg-flex">
            <div class="faixa">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/faixa-1.svg" alt="">
                <span class="sub-title">Cidades de até 50 mil habitantes</span>
                <span class="preco"> 20 mil</span>
            </div>

            <div class="faixa">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/faixa-2.svg" alt="">
                <span class="sub-title">Cidades de 50.001 a 100.000 habitantes</span>
                <span class="preco"> 30 mil</span>
            </div>

            <div class="faixa">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/faixa-3.svg" alt="">
                <span class="sub-title">Cidades de 100.001 a 250.000 habitantes</span>
                <span class="preco"> 40 mil</span>
            </div>

            <div class="faixa">
                <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/faixa-4.svg" alt="">
                <span class="sub-title">Cidades acima de 250.001 habitantes e capitais</span>
                <span class="preco"> 50 mil</span>
            </div>
        </div>

        <span class="d-lg-block d-none info">ROYALTIES 8% SOBRE O FB* / FUNDO NACIONAL DE PROPAGANDA 2% SOBRE O FB*
            PRAZO DE CONTRATO 5 ANOS / INVESTIMENTO De R$ 25 mil à R$ 97 mil</span>

    </div>

</section>




<section class="depoimentos bg-white d-block  px-4 py-5">
    <span class="title pb-lg-5">Perguntas frequentes</span>

    <div class="card col-lg-8 pb-3 py-lg-2">
        <div class="author">
            <span class="numero">1</span>
            <span class="pergunta">Como funciona o sistema de franquia?</span>
            <button class="icon closed " type="button" data-toggle="collapse" data-target="#pergunta-1" aria-expanded="false" aria-controls="pergunta-1">
            </button>
        </div>

        <div class="collapse  content" id="pergunta-1">
            <p>
                Após análise de perfil, aprovação do candidato para ser um franqueado Home Angels e pagamento da taxa de franquia, o novo franqueado inicia sua formação na universidade corporativa da franqueadora que inclui 5 dias de treinamento em nossa sede em Campinas, após isso, o empresário já esta capacitado para trabalhar como um franqueado dentro dos padrões da marca. O franqueado recebe treinamentos contantes do negócio através de nossas supervisoras, participa de encontros regionais e eventos nacionais da marca. O sistema se resume no franqueado efetuar pagamento de royalties e receber know how constante da franqueadora.


            </p>
        </div>
    </div>

    <div class="card col-lg-8 pb-3 py-lg-2">
        <div class="author">
            <span class="numero">2</span>
            <span class="pergunta">Como contratar cuidadores?</span>
            <button class="icon closed " type="button" data-toggle="collapse" data-target="#pergunta-2" aria-expanded="false" aria-controls="pergunta-2">
            </button>
        </div>

        <div class="collapse  content" id="pergunta-2">
            <p>
                Existe no portal Home Angels um banco de talentos nacional onde os nossos franqueados podem captar currículos direto de suas regiões, além disso o franqueado recebe treinamento específico na área de recrutamento e seleção de forma que se especializa em contratar profissionais para trabalhar na Home Angels.
            </p>
        </div>
    </div>

    <div class="card col-lg-8 pb-3 py-lg-2">
        <div class="author">
            <span class="numero">3</span>
            <span class="pergunta">É necessário ter ponto comercial para abrir uma Home Angels?</span>
            <button class="icon closed " type="button" data-toggle="collapse" data-target="#pergunta-3" aria-expanded="false" aria-controls="pergunta-3">
            </button>
        </div>

        <div class="collapse  content" id="pergunta-3">
            <p>
                Inicialmente você não precisará ter ponto comercial pois os atendimentos são feitos nas residências dos clientes e o recrutamento e seleção podem ser feitos em salas alugadas. A decisão de migrar para um ponto comercial tende a vir do próprio franqueado que com a flexibilidade do nosso modelo de negócio tente a retornar o investimento mais rápido e assim pode investir no seu escritório.
            </p>
        </div>
    </div>

    <div class="card col-lg-8 pb-3 py-lg-2">
        <div class="author">
            <span class="numero">4</span>
            <span class="pergunta">Posso ter um sócio para abrir minha franquia?</span>
            <button class="icon closed " type="button" data-toggle="collapse" data-target="#pergunta-4" aria-expanded="false" aria-controls="pergunta-4">
            </button>
        </div>

        <div class="collapse  content" id="pergunta-4">
            <p>
                Sim, você realizar o contrato de franquia com participação de sócios.
            </p>
        </div>
    </div>

    <div class="card col-lg-8 pb-3 py-lg-2">
        <div class="author">
            <span class="numero">5</span>
            <span class="pergunta">Preciso me dedicar em tempo exclusivo para minha franquia?</span>
            <button class="icon closed " type="button" data-toggle="collapse" data-target="#pergunta-5" aria-expanded="false" aria-controls="pergunta-5">
            </button>
        </div>

        <div class="collapse  content" id="pergunta-5">
            <p>
                Nosso negócio requer dedicação total do franqueado na operação da franquia, o mesmo deve fazer todos os treinamentos e se dedicar as atividades orientadas para busca de clientes e recrutamento de equipe. Caso o franqueado seja um investidor, deverá ter uma pessoa de confiança para operacionalizar a franquia.
            </p>
        </div>
    </div>

    <div class="card col-lg-8 pb-3 py-lg-2">
        <div class="author">
            <span class="numero">6</span>
            <span class="pergunta">Quanto posso ganhar com Home Angels?</span>
            <button class="icon closed " type="button" data-toggle="collapse" data-target="#pergunta-6" aria-expanded="false" aria-controls="pergunta-6">
            </button>
        </div>

        <div class="collapse  content" id="pergunta-6">
            <p>
                A média de lucratividade dos contratos é de 20% a 40 %, um franqueado com 1 ano de operação costuma ter em média 10 contratos. A precificação dos contratos varia conforme as despesas que o franqueado tem.
            </p>
        </div>
    </div>

    <div class="card col-lg-8 pb-3 py-lg-2">
        <div class="author">
            <span class="numero">7</span>
            <span class="pergunta">Podemos atender todos os tipos de pessoas? </span>
            <button class="icon closed " type="button" data-toggle="collapse" data-target="#pergunta-7" aria-expanded="false" aria-controls="pergunta-7">
            </button>
        </div>

        <div class="collapse  content" id="pergunta-7">
            <p>
                O modelo de negócio da Home Angels é cuidadores de pessoas, os cuidadores são profissionais treinados para cuidar da rotina dos assistidos de acordo com suas necessidades, nosso foco está na companhia e auxílio no dia a dia, portanto não atendemos pacientes de alta complexidade técnica, não podemos fazer procedimentos invasivos, o cuidador não é profissional da área da saúde, nosso franqueado é treinado para fazer a gestão de sua equipe dentro do atendimento padrão da Home Angels.
            </p>
        </div>
    </div>


</section>

<section class="formulario-franqueado">

    <div class="banner-form">
        <div class="col-lg-8 m-auto">
            <span><b>Confira</b> os passos para abrir sua franquia</span>
        </div>
    </div>
    <div class="mw-900 col-lg-9 px-0">
        <div class="box-form col-10 col-lg-6">
            <span class="pb-3">Formulário de Pré-seleção</span>
            <p>Nossa Franquia opera com exclusividade de território para fins de localização de ponto comercial e a sua escolha deverá estar livre para comercialização.</p>
            <div class="orange-box"></div>
        </div>

        <span class="sub-title d-lg-block d-none">Informe o seu território de interesse em abrir a sua franquia:</span>


        <div class="input-city d-lg-block d-none">
            <input type="text" name="cidade-abertura" placeholder="Cidade">
        </div>

        <div class="d-lg-none franq-form-mobile">
            <div class="part-1">
                <form action="">

                    <span class="sub-title ">Informe o seu território de interesse em abrir a sua franquia:</span>

                    <div class="input-city">
                        <input type="text" name="cidade-abertura-mob" placeholder="Cidade">
                    </div>

                    <input required="true" name="nme-franq-mob" type="text" placeholder="Nome completo">

                    <input class="phone" name="tel-franq-mob" required="true" type="text" placeholder="Telefone">
                    <input required="true" name="formacao-franq-mob" type="text" placeholder="Formação escolar">

                    <input class="phone" name="cel-franq-mob" required="true" type="text" placeholder="DDD + Celular">
                    <input required="true" type="text" placeholder="Cidade">

                    <input required="true" name="melhor-hora-mob" type="text" placeholder="Melhor horário para ligar (horário comercial)">
                    <select required="true" name="estado-franq-mob">
                        <option value="#" disabled selected>Estado</option>
                        <option value="AC">Acre</option>
                        <option value="AL">Alagoas</option>
                        <option value="AP">Amapá</option>
                        <option value="AM">Amazonas</option>
                        <option value="BA">Bahia</option>
                        <option value="CE">Ceará</option>
                        <option value="DF">Distrito Federal</option>
                        <option value="ES">Espírito Santo</option>
                        <option value="GO">Goiás</option>
                        <option value="MA">Maranhão</option>
                        <option value="MT">Mato Grosso</option>
                        <option value="MS">Mato Grosso do Sul</option>
                        <option value="MG">Minas Gerais</option>
                        <option value="PA">Pará</option>
                        <option value="PB">Paraíba</option>
                        <option value="PR">Paraná</option>
                        <option value="PE">Pernambuco</option>
                        <option value="PI">Piauí</option>
                        <option value="RJ">Rio de Janeiro</option>
                        <option value="RN">Rio Grande do Norte</option>
                        <option value="RS">Rio Grande do Sul</option>
                        <option value="RO">Rondônia</option>
                        <option value="RR">Roraima</option>
                        <option value="SC">Santa Catarina</option>
                        <option value="SP">São Paulo</option>
                        <option value="SE">Sergipe</option>
                        <option value="TO">Tocantins</option>
                    </select>


                    <input required="true" name="email-franq-mob" type="text" placeholder="E-mail" onblur="validateEmail(this);">

                    <select required=" true" name="como-mob">
                        <option value="" disabled selected>Como ficou sabendo da Home Angels?</option>
                        <option value="Google">Google</option>
                        <option value="Portal ABF">Portal ABF</option>
                        <option value="Redes Sociais">Redes Sociais</option>
                        <option value="Revista">Revista</option>
                        <option value="Indicação">Indicação</option>
                        <option value="Facebook">Facebook</option>
                        <option value="TV">TV</option>
                        <option value="Jornal">Jornal</option>
                        <option value="Guias">Guias</option>
                        <option value="Visitas a Feiras">Visitas a Feiras</option>
                        <option value="E-mail">E-mail</option>
                        <option value="Outros">Outros</option>
                    </select>
                </form>
                <input type="submit" name="submit-franq-mob" class="proxima-etapa d-flex d-lg-none" value="pŕoxima etapa">
            </div>

            <div class="part-2">

                <span class="form-title px-lg-0 ">Queremos saber um pouco mais sobre o seu perfil!</span>

                <form action="">
                    <div class="form-carousel">
                        <div class="indicador px-lg-0 ">
                            <span class="numero counter">1</span>
                            <span class="texto">/4</span>
                        </div>

                        <div class="question-container ">

                            <div class="question-wrapper-2">

                                <div class="question-item">
                                    <span class="question">1- Qual a sua disponibilidade para Investir na abertura da Franquia?</span>
                                    <div class="radio-inputs">

                                        <div class="resp">
                                            <input type="radio" name="question-one-mob" id="q1-r1-mob" value="25-50">
                                            <label for="q1-r1-mob">De 25 a 50 mil</label>
                                        </div>

                                        <div class="resp">
                                            <input type="radio" name="question-one-mob" id="q1-r2-mob" value="50-100">
                                            <label for="q1-r2-mob">De 50 a 100 mil</label>
                                        </div>

                                        <div class="resp">
                                            <input type="radio" name="question-one-mob" id="q1-r3-mob" value="acima-100">
                                            <label for="q1-r3-mob">Acima de 100 mil</label>
                                        </div>

                                    </div>
                                </div>

                                <div class="question-item">
                                    <span class="question">2- Qual a sua expectativa de lucro com a franquia?</span>

                                    <div class="radio-inputs">



                                        <div class="resp">
                                            <input type="radio" name="question-two-mob" id="q2-r1-mob" value="05-10">
                                            <label for="q2-r1-mob">De 5 a 10 mil</label>
                                        </div>

                                        <div class="resp">
                                            <input type="radio" name="question-two-mob" id="q2-r2-mob" value="10-20">
                                            <label for="q2-r2-mob">De 10 a 20 mil</label>
                                        </div>

                                        <div class="resp">
                                            <input type="radio" name="question-two-mob" id="q2-r3-mob" value="acima-20">
                                            <label for="q2-r3-mob">Acima de 20 mil</label>
                                        </div>

                                    </div>
                                </div>

                                <div class="question-item">
                                    <span class="question">3- Quando você deseja iniciar a franquia?</span>

                                    <div class="radio-inputs">
                                        <div class="resp">
                                            <input type="radio" name="question-three-mob" id="q3-r1-mob" value="imediatamente">
                                            <label for="q3-r1-mob">Imediatamente</label>
                                        </div>
                                        <div class="resp">
                                            <input type="radio" name="question-three-mob" id="q3-r2-mob" value="60">
                                            <label for="q3-r2-mob">Em 60 dias</label>
                                        </div>
                                        <div class="resp">
                                            <input type="radio" name="question-three-mob" id="q3-r3-mob" value="acima-60">
                                            <label for="q3-r3-mob">Acima de 60 dias</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="question-item">
                                    <span class="question">4- Quem será o responsável pela franquia?</span>

                                    <div class="radio-inputs">

                                        <div class="resp">
                                            <input type="radio" name="question-four-mob" id="q4-r1-mob" value="imediatamente">
                                            <label for="q4-r1-mob">Apenas eu</label>
                                        </div>
                                        <div class="resp">
                                            <input type="radio" name="question-four-mob" id="q4-r2-mob" value="60">
                                            <label for="q4-r2-mob">Eu e mais um sócio</label>
                                        </div>
                                        <div class="resp">
                                            <input type="radio" name="question-four-mob" id="q4-r3-mob" value="acima-60">
                                            <label for="q4-r3-mob">Grupo de sócios</label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <p class="info col-lg-6 ">Clique abaixo e envie o seu formulário e um dos nossos consultores entrará em contato com você.</p>


                        <a href="#" class="btn-franq d-flex mb-0">Enviar formulário</a>

                        <div class="question-control-desk-2 d-flex justify-content-between">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-arrowl.svg" alt="">
                            <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-arrowr.svg" alt="">
                        </div>
                    </div>
                </form>





            </div>


        </div>





        <div class="d-lg-block d-none franq-form-desk">

            <div class="">




                <form action="">

                    <input required="true" type="text" name="nome-franq" placeholder="Nome completo">

                    <div class="d-flex justify-content-between grid-franq">
                        <input class="phone" name="tel-franq" required="true" type="text" placeholder="Telefone">
                        <input required="true" name="formacao-franq" type="text" placeholder="Formação escolar">
                    </div>

                    <div class="d-flex justify-content-between grid-franq">
                        <input class="phone" name="cel-franq" required="true" type="text" placeholder="DDD + Celular">
                        <input required="true" name="cidade-franq" type="text" placeholder="Cidade">
                    </div>

                    <div class="d-flex justify-content-between grid-franq">
                        <input required="true" name="melhor-hora" type="text" placeholder="Melhor horário para ligar (horário comercial)">
                        <select required="true" name="estado-franq">
                            <option value="#" disabled selected>Estado</option>
                            <option value="AC">Acre</option>
                            <option value="AL">Alagoas</option>
                            <option value="AP">Amapá</option>
                            <option value="AM">Amazonas</option>
                            <option value="BA">Bahia</option>
                            <option value="CE">Ceará</option>
                            <option value="DF">Distrito Federal</option>
                            <option value="ES">Espírito Santo</option>
                            <option value="GO">Goiás</option>
                            <option value="MA">Maranhão</option>
                            <option value="MT">Mato Grosso</option>
                            <option value="MS">Mato Grosso do Sul</option>
                            <option value="MG">Minas Gerais</option>
                            <option value="PA">Pará</option>
                            <option value="PB">Paraíba</option>
                            <option value="PR">Paraná</option>
                            <option value="PE">Pernambuco</option>
                            <option value="PI">Piauí</option>
                            <option value="RJ">Rio de Janeiro</option>
                            <option value="RN">Rio Grande do Norte</option>
                            <option value="RS">Rio Grande do Sul</option>
                            <option value="RO">Rondônia</option>
                            <option value="RR">Roraima</option>
                            <option value="SC">Santa Catarina</option>
                            <option value="SP">São Paulo</option>
                            <option value="SE">Sergipe</option>
                            <option value="TO">Tocantins</option>
                        </select>
                    </div>


                    <div class="d-flex justify-content-between grid-franq">
                        <input required="true" name="email-franq" type="text" placeholder="E-mail" onblur="validateEmail(this);">
                        <select required="true" name="como-franq">
                            <option value="" disabled selected>Como ficou sabendo da franquia Home Angels?</option>
                            <option value="Google">Google</option>
                            <option value="Portal ABF">Portal ABF</option>
                            <option value="Redes Sociais">Redes Sociais</option>
                            <option value="Revista">Revista</option>
                            <option value="Indicação">Indicação</option>
                            <option value="Facebook">Facebook</option>
                            <option value="TV">TV</option>
                            <option value="Jornal">Jornal</option>
                            <option value="Guias">Guias</option>
                            <option value="Visitas a Feiras">Visitas a Feiras</option>
                            <option value="E-mail">E-mail</option>
                            <option value="Outros">Outros</option>
                        </select>
                    </div>

                    <div class="mt-desconto">
                        <span class="form-title px-lg-0 col-lg-7">Queremos saber um pouco mais sobre o seu perfil!</span>

                        <div class="form-carousel">
                            <div class="indicador px-lg-0 col-lg-5">
                                <span class="numero counter">1</span>
                                <span class="texto">de 4</span>
                            </div>

                            <div class="question-container ">

                                <div class="question-wrapper">

                                    <div class="question-item">
                                        <span class="question">1- Qual a sua disponibilidade para Investir na abertura da Franquia?</span>
                                        <div class="radio-inputs">

                                            <div class="resp">
                                                <input type="radio" name="question-one" id="q1-r1" value="25-50">
                                                <label for="q1-r1">De 25 a 50 mil</label>
                                            </div>

                                            <div class="resp">
                                                <input type="radio" name="question-one" id="q1-r2" value="50-100">
                                                <label for="q1-r2">De 50 a 100 mil</label>
                                            </div>

                                            <div class="resp">
                                                <input type="radio" name="question-one" id="q1-r3" value="acima-100">
                                                <label for="q1-r3">Acima de 100 mil</label>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="question-item">
                                        <span class="question">2- Qual a sua expectativa de lucro com a franquia?</span>

                                        <div class="radio-inputs">



                                            <div class="resp">
                                                <input type="radio" name="question-two" id="q2-r1" value="05-10">
                                                <label for="q2-r1">De 5 a 10 mil</label>
                                            </div>

                                            <div class="resp">
                                                <input type="radio" name="question-two" id="q2-r2" value="10-20">
                                                <label for="q2-r2">De 10 a 20 mil</label>
                                            </div>

                                            <div class="resp">
                                                <input type="radio" name="question-two" id="q2-r3" value="acima-20">
                                                <label for="q2-r3">Acima de 20 mil</label>
                                            </div>

                                        </div>
                                    </div>

                                    <div class="question-item">
                                        <span class="question">3- Quando você deseja iniciar a franquia?</span>

                                        <div class="radio-inputs">
                                            <div class="resp">
                                                <input type="radio" name="question-three" id="q3-r1" value="imediatamente">
                                                <label for="q3-r1">Imediatamente</label>
                                            </div>
                                            <div class="resp">
                                                <input type="radio" name="question-three" id="q3-r2" value="60">
                                                <label for="q3-r2">Em 60 dias</label>
                                            </div>
                                            <div class="resp">
                                                <input type="radio" name="question-three" id="q3-r3" value="acima-60">
                                                <label for="q3-r3">Acima de 60 dias</label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="question-item">
                                        <span class="question">4- Quem será o responsável pela franquia?</span>

                                        <div class="radio-inputs">

                                            <div class="resp">
                                                <input type="radio" name="question-four" id="q4-r1" value="imediatamente">
                                                <label for="q4-r1">Apenas eu</label>
                                            </div>
                                            <div class="resp">
                                                <input type="radio" name="question-four" id="q4-r2" value="60">
                                                <label for="q4-r2">Eu e mais um sócio</label>
                                            </div>
                                            <div class="resp">
                                                <input type="radio" name="question-four" id="q4-r3" value="acima-60">
                                                <label for="q4-r3">Grupo de sócios</label>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <p class="info col-lg-6 ">Clique abaixo e envie o seu formulário e um dos nossos consultores entrará em contato com você.</p>


                    <input type="submit" name="submit-franq" class="btn-franq d-flex mb-0" value="Enviar meu formulário de pré-seleção">
                </form>





                <div class="question-control-desk d-flex justify-content-between">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-arrowl.svg" alt="">
                    <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/dif-arrowr.svg" alt="">
                </div>

            </div>
        </div>
    </div>



</section>

<section id="encontre">
    <a href="#" class="d-lg-none franq-click d-flex encontre">
        Quero ser franqueado</a>
</section>

<?php get_template_part('footer-extra'); ?>

<?php get_footer(); ?>