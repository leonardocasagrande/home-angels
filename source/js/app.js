function openNav() {
  document.getElementById("mySidenav").style.width = "252px";
  $(".desk-nav").toggleClass("desk-active");
  setTimeout(function () {
    $(".sidenav").addClass("overflow-initial");
  }, 300);
}

function closeNav() {
  $(".sidenav").removeClass("overflow-initial");
  document.getElementById("mySidenav").style.width = "0";
}

$(".back-to-top").click(function (e) {
  e.preventDefault();
  var body = $("html, body");
  body.stop().animate({ scrollTop: 0 }, 500, "swing");
});

$(".author button").click(function () {
  this.classList.toggle("closed");
  this.classList.toggle("opened");
  this.parentElement.parentElement.classList.toggle("without-span");
});

$(window).on("scroll", function () {
  var scrollHeight = $(document).height();
  var scrollPosition = $(window).height() + $(window).scrollTop();
  if ((scrollHeight - scrollPosition) / scrollHeight === 0) {
    $("#encontre").addClass("d-none");
  } else {
    $("#encontre").removeClass("d-none");
  }
});

$(".myBtn").click(function () {
  let parent = this.parentElement.children[0];
  var dots = parent.children[1];
  var moreText = parent.children[2];

  if (dots.style.display === "none") {
    dots.style.display = "inline";
    this.innerHTML = "Ver mais";
    moreText.style.display = "none";
  } else {
    dots.style.display = "none";
    this.innerHTML = "Ver menos";
    moreText.style.display = "inline";
  }
});

$(".encontre-unidade #cidade").change(function () {
  if (this.value === "sao-paulo") {
    $("#regiao").attr("style", "display: block");
  } else {
    $("#regiao").attr("style", "display: none");
  }
});

$(document).ready(function () {
  $(".back-button").click(function (e) {
    e.preventDefault();
    window.history.back();
  });
});

$(document).ready(function () {
  $(".unidade-item .name").click(function () {
    window.location.href = "http://humann.com.br/home-angels/agendamento";
  });
});

$(".covid-btn").click(function () {
  $(this).toggleClass("fit-height");
  console.log("click");
});

// CARROSSÉIS
$(document).ready(function () {
  var banners = tns({
    container: ".carosel-banners",
    mode: "carousel",
    items: 1,
    slideBy: 1,
    autoplay: true,
    loop: true,
    nav: false,
    controls: false,
    touch: false,
    navPosition:'bottom',
    responsive:{
      1024:{
        nav:true
      }
    }
  });
});
$(document).ready(function () {
  if (window.innerWidth < 800) {
    var unidades = tns({
      container: ".container-unidades",
      center: true,
      fixedWidth: 322,
      items: 1,
      slideBy: 1,
      autoplay: false,
      loop: false,
      navContainer: ".wrapper-unidade",
      controls: false,
      center: true,
      gutter: 30,
    });
  } else {
    var unidadesDesk = tns({
      container: ".container-unidades",
      center: true,
      mode: "gallery",
      items: 1,
      slideBy: "page",
      autoplay: false,
      loop: false,
      navContainer: ".wrapper-unidade",
      controls: false,
      center: true,
      gutter: 30,
      navAsThumbnails: true,
    });

    var thumbDesk = tns({
      container: ".wrapper-unidade",
      mode: "carousel",
      center: true,
      items: 3,
      slideBy: 1,
      autoplay: false,
      nav: true,
      mouseDrag: true,
      controls: false,
      // controlsContainer: ".unidade-control",
    });
  }
});

$(document).ready(function () {
  var franqueadoMobile = tns({
    container: ".wrapper",
    mode: "carousel",
    items: 1,
    slideBy: 1,
    autoplay: false,
    loop: true,
    nav: false,
    controlsContainer: ".unidade-control",
    touch: false,
  });
});

$(document).ready(function () {
  var funcoes = tns({
    container: ".funcoes-container",
    mode: "carousel",
    items: 1,
    slideBy: 1,
    autoplay: false,
    loop: true,
    nav: false,
    controlsContainer: ".funcoes-nav",
    touch: false,
  });
});

$(document).ready(function () {
  var resgate = tns({
    container: ".resgate-container",
    mode: "carousel",
    items: 1,
    slideBy: 1,
    autoplay: false,
    loop: false,
    nav: false,
    controlsContainer: ".vantagens-nav",
  });
});

$(document).ready(function () {
  var palavraMobile = tns({
    container: ".palavra-wrapper ",
    mode: "carousel",
    items: 1,
    slideBy: 1,
    autoplay: false,
    loop: true,
    nav: false,
    center: true,
    controlsContainer: ".palavra-nav",
  });
});
$(document).ready(function () {
  var depoimentos = tns({
    container: ".card-container",
    items: 3,
    slideBy: 1,
    gutter: 20,
    loop: false,
    autoplay: true,
    autoplayHoverPause: true,
    autoplayButtonOutput: false,
    mode: "carousel",
    controlsContainer: ".controls",
    nav: true,
    navPosition: "bottom",
  });
});
$(document).ready(function () {
  var excelencia = tns({
    container: ".excelencia-wrapper ",
    mode: "carousel",
    items: 1,
    slideBy: 1,
    autoplay: false,
    loop: true,
    center: true,
    controls: false,
    navContainer: ".dots-excelencia",
  });
});
$(document).ready(function () {
  var razoes = tns({
    container: ".razoes-container",
    mode: "carousel",
    items: 1,
    slideBy: 1,
    autoplay: false,
    loop: true,
    center: true,
    controlsContainer: ".razoes-nav",
    nav: false,
  });
});

$(document).ready(function () {
  var equipe = tns({
    container: ".equipe-container",
    mode: "carousel",
    items: 1,
    slideBy: 1,
    autoplay: false,
    loop: true,
    center: true,
    controlsContainer: ".espec-nav",
    nav: false,
  });
});

$(document).ready(function () {
  var acompTerap = tns({
    container: ".at-container",
    mode: "carousel",
    items: 1,
    slideBy: 1,
    autoplay: false,
    loop: false,
    controls: false,
    nav: true,
    responsive: {
      300: {
        disable: false,
        fixedWidth: 300,
        center: true,
      },
      1000: {
        disable: true,
      },
    },
  });
});

$(document).ready(function () {
  var at = tns({
    container: ".at-container",
    mode: "carousel",
    items: 1,
    slideBy: 1,
    autoplay: false,
    loop: true,
    center: true,
    controlsContainer: ".at-control",
    nav: false,
    gutter: 10,
    edgePadding: 10,
    responsive: {
      1000: {
        disable: true,
      },
    },
  });
});
$(document).ready(function () {
  var blogHome = tns({
    container: ".blog-home-wrapper ",
    mode: "carousel",
    slideBy: 1,
    autoplay: false,
    loop: true,
    controlsContainer: ".blog-home-nav",
    navContainer: ".blog-home-dots",
    responsive: {
      300: {
        center: true,
        items: 1,
        // fixedWidth: 246,
      },
      1000: {
        items: 3,
      },
    },
  });
  
});
$(document).ready(function(){
  var artigosCovid = tns({
    container: ".artigos-container",
    mode: "carousel",
    slideBy: 1,
    autoplay: false,
    loop: false,
    controlsContainer: ".artigos-nav",
    nav:false,
    responsive: {
      300: {
        center: true,
        items: 1,
        // fixedWidth: 246,
      },
      1000: {
        center: false,
        items: 3,
      },
    },
  });
});
$(document).ready(function () {
  var franqueadoDesk = tns({
    container: ".franq-wrapper",
    mode: "carousel",
    items: 2,
    slideBy: "page",
    autoplay: false,
    loop: true,
    // nav: false,
    navContainer: ".dots-franq-desk",
    controlsContainer: ".unidade-control-desk",
    touch: false,
    gutter: 30,
  });
});

$(document).ready(function () {
  var franqueadoDifMobile = tns({
    container: ".dif-wrapper",
    mode: "carousel",
    slideBy: 1,
    items: 1,
    autoplay: false,
    loop: false,
    navPosition: "bottom",
    controlsContainer: ".dif-nav",
    touch: true,
    center: false,
    gutter: 45,
    // edgePadding: 45,
    responsive: {
      1000: {
        items: 4,
      },
    },
  });
});

$(document).ready(function () {
  var casesSucesso = tns({
    container: ".video-container",
    mode: "carousel",
    items: 3,
    center: false,
    slideBy: 1,
    autoplay: false,
    loop: true,
    nav: false,
    controlsContainer: ".video-nav",
    touch: false,
    gutter: 20,
  });
});

$(document).ready(function () {
  var formIntoForm = tns({
    container: ".question-wrapper-2",
    mode: "carousel",
    items: 1,
    slideBy: 1,
    autoplay: false,
    nav: false,
    loop: false,
    controlsContainer: ".question-control-desk-2",
  });

  $(".question-control-desk-2").click(function () {
    let info = formIntoForm.getInfo().displayIndex;
    if (info <= 4) {
      var count = info.toString();
      $(".counter").text(count);
    } else {
      $(".counter").text(1);
      formIntoForm.goTo(0);
    }
    if (info === 4) {
      $(".question-control-desk-2 img:last-child").addClass("d-none");
    } else {
      $(".question-control-desk-2 img:last-child").removeClass("d-none");
    }
  });
});

$(document).ready(function () {
  var formIntoForm = tns({
    container: ".question-wrapper",
    mode: "carousel",
    items: 1,
    slideBy: 1,
    autoplay: false,
    nav: false,
    loop: false,
    controlsContainer: ".question-control-desk",
  });

  $(".question-control-desk").click(function () {
    let info = formIntoForm.getInfo().displayIndex;
    if (info <= 4) {
      var count = info.toString();
      $(".counter").text(count);
    } else {
      $(".counter").text(1);
      formIntoForm.goTo(0);
    }
  });
});

$("#form-unity").validate({
  rules: {
    phone: {
      required: true,
      minlength: 14,
    },
  },
  messages: {
    email: "Digite um email válido",
    customer_name: "Digite seu nome corretamente",
    phone: {
      required: "Digite seu telefone corretamente",
      minlength: "Digite seu telefone corretamente",
    },
  },
  submitHandler: function (form) {
    var formdata = $(form).serialize();

    $.ajax({
      url: "/home-angels/email-mailer/?" + formdata,
      beforeSend: function () {
        $(".loading").addClass("d-block").removeClass("d-none");
        $(".btn-form").attr("type", "hidden");
      },
    })
      .done(function (msg) {
        $(".btn-form").attr("type", "submit");
        $(".loading").removeClass("d-block");
        $(".loading").addClass("d-none");

        let url = `/home-angels/agendamento/?${formdata}`;

        window.location.href = url;
      })
      .fail(function (jqXHR, textStatus, msg) {
        alert("Ocorreu uma falha, tente mais tarde");
      });
  },
});

$("#form-mobile").validate({
  rules: {
    phone: {
      required: true,
      minlength: 14,
    },
  },
  messages: {
    email: "Digite um email válido",
    customer_name: "Digite seu nome corretamente",
    phone: {
      required: "Digite seu telefone corretamente",
      minlength: "Digite seu telefone corretamente",
    },
  },
  submitHandler: function (form) {
    var formdata = $(form).serialize();

    $.ajax({
      url: "/home-angels/email-mailer/?" + formdata,
      beforeSend: function () {
        $(".loading").addClass("d-block").removeClass("d-none");
        $(".btn-form").attr("type", "hidden");
      },
    })
      .done(function (msg) {
        $(".btn-form").attr("type", "submit");
        $(".loading").removeClass("d-block");
        $(".loading").addClass("d-none");

        let url = `/home-angels/agendamento/?${formdata}`;

        window.location.href = url;
      })
      .fail(function (jqXHR, textStatus, msg) {
        alert("Ocorreu uma falha, tente mais tarde");
      });
  },
});

$("#form-customer").validate({
  rules: {
    phone: {
      required: true,
      minlength: 14,
    },
  },
  messages: {
    email: "Digite um email válido",
    customer_name: "Digite seu nome corretamente",
    phone: {
      required: "Digite seu telefone corretamente",
      minlength: "Digite seu telefone corretamente",
    },
  },
  submitHandler: function (form) {
    var formData = $(form).serialize();

    sessionStorage.setItem("customer", formData);

    sessionStorage.removeItem("unity");

    window.location.href = "/home-angels/unidades-form";
  },
});

function loadAddress(type, params, disabled) {
  let url = `https://humann.com.br/home-angels/wp-json/acf/v3/${type}`;
  let selectName = $(`#${type}`);

  $.ajax({
    url: `https://humann.com.br/home-angels/wp-json/acf/v3/${type}`,
    data: {
      per_page: 9999,
    },
    beforeSend: function () {
      $(".loading").addClass("d-block").removeClass("d-none");
      $(".btn-form").attr("type", "hidden");
    },
  })
    .done(function (data) {
      $(".btn-form").attr("type", "submit");
      $(".loading").removeClass("d-block");
      $(".loading").addClass("d-none");

      let location = [];

      $(`#${type} option`).each(function () {
        const attr = $(this).attr("disabled");

        if (attr != "disabled") {
          $(this).remove();
        }
      });

      let paramSort;

      switch (type) {
        case "states":
          paramSort = "states_name";
          break;

        case "cities":
          paramSort = "city_name";
          break;

        case "zones":
          paramSort = "zone_name";
          break;

        default:
          break;
      }

      data.sort(function (a, b) {
        var textA = a.acf[paramSort].toUpperCase();
        var textB = b.acf[paramSort].toUpperCase();
        return textA < textB ? -1 : textA > textB ? 1 : 0;
      });

      if (params) {
        let property;

        type === "cities"
          ? (property = "city_state")
          : (property = "zone_city");

        data = data.filter((value) => value.acf[property] == params && data);
      }

      appendToSession(`store-${type}`, data);

      if (data.length) {
        data.map((value) => {
          value.value = Object.values(value.acf)[0];

          location = [...location, value];

          var option = document.createElement("option");
          option.text = value.value;
          option.value = value.id;
          var select = document.getElementById(`${type}`);
          select.appendChild(option);
        });

        selectName.removeAttr("disabled");
      }
    })
    .fail(function (jqXHR, textStatus, msg) {
      alert("Ocorreu uma falha, tente mais tarde");
    });
}

function appendToSession(key, value) {
  var item = JSON.parse(sessionStorage.getItem(key));

  if (!item) {
    sessionStorage.setItem(key, JSON.stringify(value));
    return;
  }

  let newValue = [...item, value];

  sessionStorage.setItem(key, JSON.stringify(newValue));
}

$(document).ready(function () {
  const selectAddress = $(".select-address");

  const placeholder = selectAddress.data("placeholder");

  selectAddress.select2({
    placeholder: {
      id: "-1",
      text: placeholder,
    },
  });

  $(".select-address").on("change", function (data) {
    let value = data.target.value;
    let dataSelected = $(this).data("nextStep");

    if (dataSelected == "zones") {
      $("#btn-find-unity").removeAttr("disabled");
      $("#btn-find-unity").removeClass("disabled");
    }

    if (value == 315 || dataSelected == false) {
      $("#zone-form").removeClass("d-none");
    } else {
      $("#zone-form").addClass("d-none");
    }

    if (dataSelected) {
      loadAddress(dataSelected, value);
    }
  });
});

$("#find-unity").on("submit", function (e) {
  e.preventDefault();

  var formdata = $(this).serializeArray();

  var data = {};

  $(formdata).each(function (index, obj) {
    data[obj.name] = obj.value;
  });

  sessionStorage.setItem("unity", JSON.stringify(data));

  if (window.location.href.includes("unidades-form")) {
    return (window.location.href = "/home-angels/unidades-form#selects-params");
  }

  window.location.href = "/home-angels/nossas-unidades#selects-params";
});

function removeDuplicates(array, key) {
  return array.reduce((accumulator, element) => {
    if (!accumulator.find((el) => el[key] === element[key])) {
      accumulator.push(element);
    }
    return accumulator;
  }, []);
}

function handleUnits() {
  let paramsUnits = JSON.parse(sessionStorage.getItem("unity"));

  paramsUnits = Object.values(paramsUnits);

  paramsUnits = parseInt(paramsUnits.slice(-1).pop());

  $.ajax({
    url: "https://humann.com.br/home-angels/wp-json/acf/v3/units",
    data: { per_page: 9999 },
    beforeSend: function () {
      $(".loading").addClass("d-block").removeClass("d-none");
      $(".btn-form").attr("type", "hidden");
    },
  })
    .done(function (data) {
      $(".btn-form").attr("type", "submit");
      $(".loading").removeClass("d-block");
      $(".loading").addClass("d-none");

      let unitys = [];

      data.map((unity) =>
        Object.values(unity.acf).find(
          (attr) => attr === paramsUnits && (unitys = [...unitys, unity])
        )
      );

      unitys = removeDuplicates(unitys, "id");

      console.log(unitys);

      let content;

      if (!unitys.length) {
        content = `<div class="unidade-item  px-0">
      Nenhuma unidade Encontrada
    </div>`;
      } else {
        content = `
    
      ${unitys
        .map(
          (element) => `<div class="unidade-item  px-0">
      <a href="/home-angels/?page_id=${element.id}" target="_blank" class="name">${element.acf.unit_name}</a>

      <div>
          <a target="_blank" href="https://api.whatsapp.com/send?phone=${element.acf.unit_whatsapp}"><i class="fab icon-unity icon-wpp fa-whatsapp"></i></a>
          <span class="divisor"></span>
          <a target="_blank" href="tel:${element.acf.unit_phone}"><i class="fas icon-phone  icon-unity fa-phone-alt"></i></a>
      </div>
  </div>
`
        )
        .join("")}`;
      }

      $("#unidade-container").append(content);
    })
    .fail(function (jqXHR, textStatus, msg) {
      alert("Ocorreu uma falha, tente mais tarde");
    });
}

function handleUnitsForm() {
  let paramsUnits = JSON.parse(sessionStorage.getItem("unity"));

  paramsUnits = Object.values(paramsUnits);

  paramsUnits = parseInt(paramsUnits.slice(-1).pop());

  $.ajax({
    url: "https://humann.com.br/home-angels/wp-json/acf/v3/units",
    data: { per_page: 9999 },
    beforeSend: function () {
      $(".loading").addClass("d-block").removeClass("d-none");
      $(".btn-form").attr("type", "hidden");
    },
  })
    .done(function (data) {
      $(".btn-form").attr("type", "submit");
      $(".loading").removeClass("d-block");
      $(".loading").addClass("d-none");

      let unitys = [];

      data.map((unity) =>
        Object.values(unity.acf).find(
          (attr) => attr === paramsUnits && (unitys = [...unitys, unity])
        )
      );

      /*      unitys = removeDuplicates(unitys, "id"); */

      let content;

      if (!unitys.length) {
        content = `<div class="unidade-item  px-0">
      Nenhuma unidade Encontrada
    </div>`;
      } else {
        content = `
    
      ${unitys
        .map(
          (element) => `<div class="unidade-item  px-0">
      <a class="name unity-box"   data-id="${element.id}" data-unity-email="${element.acf.unit_email}" >${element.acf.unit_name}</a>

      <div>
          <a target="_blank" href="https://api.whatsapp.com/send?phone=${element.acf.unit_whatsapp}"><i class="fab icon-unity icon-wpp fa-whatsapp"></i></a>
          <span class="divisor"></span>
          <a target="_blank" href="tel:${element.acf.unit_phone}"><i class="fas icon-phone  icon-unity fa-phone-alt"></i></a>
      </div>
  </div>
`
        )
        .join("")}`;
      }

      $("#unidade-container").append(content);

      $(".unity-box").click(function (e) {
        e.preventDefault();
        const idUnity = $(this).data("id");
        const unityemail = $(this).data("unityEmail");

        let customer = sessionStorage.getItem("customer");

        customer = customer + `&unity_email=${unityemail}&unity_id=${idUnity}`;

        handleEmail(customer);
      });
    })
    .fail(function (jqXHR, textStatus, msg) {
      alert("Ocorreu uma falha, tente mais tarde");
    });
}

function handleEmail(data) {
  $.ajax({
    url: "/home-angels/email-mailer/?" + data,
    beforeSend: function () {
      $(".loading").addClass("d-block").removeClass("d-none");
      $(".btn-form").attr("type", "hidden");
    },
  })
    .done(function (msg) {
      $(".btn-form").attr("type", "submit");
      $(".loading").removeClass("d-block");
      $(".loading").addClass("d-none");

      let url = `/home-angels/agendamento/?${data}`;

      sessionStorage.removeItem("customer");

      window.location.href = url;
    })
    .fail(function (jqXHR, textStatus, msg) {
      alert("Ocorreu uma falha, tente mais tarde");
    });
}

/* function preLoadSelect() {
  let params = JSON.parse(sessionStorage.getItem("unity"));

  if (params) {
    Object.entries(params).map(([key, value]) => {
      $(`#${key}`).select2("val", value).val(parseInt(value)).trigger("change");
    });
  }
}

$(document).ready(function () {
  preLoadSelect();
}); */

$("#depoimentos-desktop .leia-mais").on("click", function (e) {
  e.preventDefault();
  if ($(".btn-fechar").hasClass("d-none")) {
    $(".btn-fechar").removeClass("d-none");
  } else {
    $(".btn-fechar").addClass("d-none");
  }
  $("#depoimentos-desktop .collapse").collapse("toggle");
});

$(".proxima-etapa").click(function (e) {
  e.preventDefault();
  $(".part-1").addClass("off");
  $(".part-2").addClass("on");
});

$(".franq-click").click(function (e) {
  e.preventDefault();
  let form = document.body.scrollHeight - 1200;
  window.scrollTo(0, form);
  console.log(form);
});

//MASCARAS
$(".phone").inputmask({
  placeholder: "",
  mask: function () {
    return ["(99) 9999-9999", "(99) 99999-9999"];
  },
});

$(".cep").inputmask({
  placeholder: "",
  mask: function () {
    return "99999-999";
  },
});

$(".data-nasc").inputmask({
  placeholder: "",
  mask: function () {
    return "99/99/9999";
  },
});

function validateEmail(emailField) {
  var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

  if (reg.test(emailField.value) == false) {
    alert("Email Inválido");
    return false;
  }

  return true;
}

// SEARCH BAR
var url = document.location.origin + "/home-angels";

// $("#searchButton").click(function (e) {
//   e.preventDefault();
//   // $(".post-container").remove();
//   let inputValue = $("#searchValue").val();

//   const retorno = $.ajax({
//     url: `${url}/wp-json/wp/v2/posts?_embed`,
//     data: {
//       search: inputValue,
//     },
//     beforeSend: function () {
//       $(".search-response").empty();
//       $(".post-container").empty();
//       let loadingIcon = `
//       <div class="loading">
//         <div class="loader"></div>
//       </div>`;
//       console.log(retorno);
//       $(".search-response").append(loadingIcon);
//     },
//   }).done(function (data) {
//     let emptyContent = `<div style="height:400px"><p>Não encontramos o que você procura</p></div>`;

//     if (data.length == 0) {
//       $(".search-response").empty();
//       $(".search-response").append(emptyContent);
//       $("#loadMore").remove();
//     }
//     // console.log(data.length)

//     $(".loading").remove();
//     let content = `
//         ${data
//           .map(
//             (element) => `
//         <a class="">
//          ${element.title}
//         </a>`
//           )
//           .join("")}`;

//     $(".search-response").append(content);
//   });
// });

// $(".ver-mais").click(function (e) {
//   e.preventDefault();
//   let pageNumber = 1;
//   $.ajax({
//     url: `${url}/wp-json/wp/v2/posts?per_page=1&page=${pageNumber}`,
//     data: {},
//     statusCode: {
//       200: function (data, status, xhr) {
//         const numPosts = xhr.getResponseHeader("X-WP-TotalPages");

//         let content = `
//         ${data
//           .map(
//             (element) => `
//             <div style="height:400px; background:red"></div>`
//           )
//           .join("")}`;
//         if (pageNumber >= numPosts) {
//           $(".ver-mais").remove();
//         }

//         $(".post-container").append(content);
//       },
//     },
//   });
// });
