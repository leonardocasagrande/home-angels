<?php
//Template Name: unidades
define('DONOTCACHEPAGE', true);

get_header(); ?> <section class="banner-unidades"><div class="banner-1 d-none d-lg-block bg-sobre"><div class="filter-white blue"><span class="d-none title d-lg-block">Nossas Unidades</span></div></div><div class="textura pt-lg-4" id="selects-params"><div class="col-10 col-lg-4 text"><span class="hello">Olá,</span> <span class="escolha">veja abaixo as unidades mais próximas da cidade selecionada.</span></div><div class="square d-lg-none"><div></div></div></div><div class="container unity-mobile"><div class="pt-5 pb-5 unidade px-4"><form id="find-unity"><div class="row align-items-center justify-content-center"><div class="col-md-3"><select name="states" class="select-address" id="states" data-placeholder="Estado" data-next-step="cities"><option value="" disabled="disabled" selected="selected">Estado</option></select></div><div class="col-md-3"><select name="cities" id="cities" class="select-address" data-next-step="zones" data-placeholder="Cidade" disabled="disabled"><option value="" disabled="disabled" selected="selected">Cidade</option></select></div><div class="col-md-3 d-none" id="zone-form"><select name="zones" class="select-address" id="zones" data-placeholder="Zona" disabled="disabled"><option value="" disabled="disabled" selected="selected">Zona</option></select></div><div class="col-md-3 btn-un-form-mob text-align-left"><button type="submit" id="btn-find-unity" class="btn-geral m-1 disabled" disabled="disabled">buscar</button></div></div></form></div></div><div class="unidade-intermediaria d-lg-block"><div id="unidade-container" class="unidade-container col-lg-8 col-12"></div></div></section> <?php get_template_part('footer-extra'); ?> <?php get_footer(); ?> <?php


if ($_GET['clean'] === 'true') {

?> <script>sessionStorage.removeItem("unity")</script> <?php
};

?> <script>loadAddress("states");
    handleUnits();</script>