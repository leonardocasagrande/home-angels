<?php get_header(); ?> <section><div class="bg-red"><div class="banner container col-lg-8 px-lg-0 text-white"><h2>Home / Produtos</h2><h1>Produtos</h1></div></div></section><section class="px-4 py-5 text-center"><span class="title d-lg-none px-2">Um mercado impulsionado pela busca de mais sabor, qualidade e competitividade</span><div class="card-container d-lg-flex justify-content-around m-lg-auto flex-wrap py-5 col-12 col-lg-10"><div class="dropdown card-green col-lg-5"><a class="dropdown-toggle" href="#" role="button" id="substitutoQueijo" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="text-left">Substitutos<br>de queijo</span> <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/green-right.svg" alt=""> </a><!-- <div class="dropdown-menu " aria-labelledby=" substitutoQueijo">


                <div class="dropdown-item ">
                    <img src=" <?= get_stylesheet_directory_uri(); ?>/dist/img/mozzana.svg" alt="">
                    <p>Desenvolvida para ralar ou filetar e misturar ao queijo mussarela.</p>

                    <a class="btn-green" href="#">Saiba mais</a>
                </div>

                <div class="dropdown-item">
                    <img src=" <?= get_stylesheet_directory_uri(); ?>/dist/img/mozza.png" alt="">
                    <p>Desenvolvida para ralar ou filetar e misturar ao queijo mussarela.</p>

                    <a class="btn-green" href="#">Saiba mais</a>
                </div>

                <div class="dropdown-item ">
                    <img src=" <?= get_stylesheet_directory_uri(); ?>/dist/img/mozzana.svg" alt="">
                    <p>Desenvolvida para ralar ou filetar e misturar ao queijo mussarela.</p>

                    <a class="btn-green" href="#">Saiba mais</a>
                </div>

            </div> --></div><div class="dropdown card-blue mt-3 col-lg-5"><a class="dropdown-toggle" href="#" role="button" id="coberturaCremosa" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="text-left">Coberturas cremosas<br>sabor requeijão</span> <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/blue-right.svg" alt=""></a></div><div class="dropdown card-red mt-3 col-lg-5"><a class="dropdown-toggle" href="#" role="button" id="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="text-left">Compostos<br>lácteos</span> <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/red-right.svg" alt=""></a></div><div class="dropdown card-yellow mt-3 col-lg-5"><a class="dropdown-toggle" href="#" role="button" id="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="text-left">Produtos para<br>vending machine</span> <img src="<?= get_stylesheet_directory_uri(); ?>/dist/img/yellow-right.svg" alt=""></a></div></div></section><section class="pb-5"><div class="fale-conosco col-10 box-radius px-4 py-5"><span class="pb-5">Fale Conosco</span> <a href="#">entrar em contato</a></div></section> <?php get_footer(); ?>